
This document describes the changes in each version of chicken-sdl2.

This library follows [semantic versioning](http://semver.org).
Until version 1.0.0 is released, the maintainer reserves the right to
change the API if needed, possibly in ways that break compatibility
with previous versions. **Large backward incompatible changes are
unlikely**, but there may be small tweaks and fixes to the API if
problems are discovered.

After version 1.0.0 is released, the API is guaranteed to remain
stable (no backward incompatible changes) until the next new major
version (e.g. going from version 1.x to 2.0.0, or 2.x to 3.0.0).


# 0.4.1 (2021-10-21)

- Fixed several compile errors about undefined constants or functions
  when using old versions of SDL2.
- Fixed incorrect type declarations for `controller-axis-event-axis`
  and `controller-button-event-button`.
- chicken-sdl2 no longer depends on the miscmacros egg.


# 0.4.0 (2021-10-15)

## Backward Incompatible Changes

- The list of symbols returned by `renderer-info-texture-formats` now
  contains the symbol `unknown` once for each texture format with an
  unknown name. This can happen when a new texture format was added to
  SDL, but has not yet been added to chicken-sdl2. Previously, this
  would cause an error.

- `renderer-info-texture-formats-raw` now returns a list of integers
  instead of a pointer to a C array of integers.

- `controller-axis-event-axis` now returns a controller axis enum
  symbol. Use `controller-axis-event-axis-raw` to get an integer.

- `controller-button-event-button` now returns a controller button
  enum symbol. Use `controller-button-event-button-raw` to get an
  integer.

- `wait-event!` and `wait-event-timeout!` are now based on `delay!`
  by default, instead of SRFI-18's `thread-sleep!` procedure. If you
  are using SRFI-18 multithreading, you should provide a compatible
  delay procedure based on `thread-sleep!`. See wiki docs for details.

- Removed procedures which have been deprecated since chicken-sdl2 0.2.0:

      copy-color          copy-color*
      copy-colour         copy-colour*
      copy-point          copy-point*
      copy-rect           copy-rect*


## Bug Fixes

- Fixed several joystick procedures accidentally returning nothing.
  Thanks to megane for reporting this. Affected procedures:

      num-joysticks
      joystick-num-axes
      joystick-num-balls
      joystick-num-buttons
      joystick-num-hats

- Fixed `palette-set!` (and using `set!` with `palette-ref`) not
  behaving as expected. Thanks to kp for discovering this.


## New Features

Many new features have been added. Features which require a version of
SDL greater than 2.0.0 are marked with the required version.

### Cursor

New procedures:

```
cursor?
create-cursor                 create-cursor*
create-color-cursor           create-color-cursor*
create-colour-cursor          create-colour-cursor*
create-system-cursor          create-system-cursor*
free-cursor!
get-cursor                    cursor-set!
get-default-cursor
show-cursor?                  show-cursor!
```

### Display / Video Drivers

New procedures:

```
video-init!
video-quit!
get-num-video-drivers
get-video-driver
get-current-video-driver

get-num-video-displays
get-display-name
get-display-dpi                                   (SDL 2.0.4+)
get-display-bounds
get-display-usable-bounds                         (SDL 2.0.5+)
get-display-orientation                           (SDL 2.0.9+)
get-display-orientation-raw                       (SDL 2.0.9+)

get-num-display-modes
get-display-mode
get-current-display-mode
get-closest-display-mode
get-desktop-display-mode
```

### Events

- Added `display-event` event type. (SDL 2.0.9+)
- Added `drop-text`, `drop-begin`, and `drop-complete` event types. (SDL 2.0.5+).
- Added `take-focus` and `hit-test` window events (SDL 2.0.5+).

New procedures:

```
display-event?                                    (SDL 2.0.9+)
display-event-display                             (SDL 2.0.9+)
display-event-display-set!                        (SDL 2.0.9+)
display-event-event                               (SDL 2.0.9+)
display-event-event-raw                           (SDL 2.0.9+)
display-event-event-set!                          (SDL 2.0.9+)
display-event-data1                               (SDL 2.0.9+)
display-event-data1-set!                          (SDL 2.0.9+)
display-event-orientation                         (SDL 2.0.9+)
display-event-orientation-set!                    (SDL 2.0.9+)

drop-event-window-id                              (SDL 2.0.5+)
drop-event-window-id-set!                         (SDL 2.0.5+)
```

### Game Controller

Added the **sdl2:game-controller** and **sdl2:game-controller-button-bind**
struct record types and many function bindings related to
[game controllers](https://wiki.libsdl.org/CategoryGameController). [#41]

New procedures:

```
game-controller?
game-controller-open!
game-controller-close!
game-controller-from-instance-id                  (SDL 2.0.4+)
game-controller-update!

game-controller-event-state?
game-controller-event-state-set!

game-controller-attached?
game-controller-get-joystick
is-game-controller?

game-controller-mapping
game-controller-mapping-for-guid
game-controller-add-mapping!
game-controller-add-mappings-from-file!           (SDL 2.0.2+)
game-controller-add-mappings-from-rw!             (SDL 2.0.2+)
game-controller-num-mappings                      (SDL 2.0.6+)
game-controller-mapping-for-index                 (SDL 2.0.6+)
game-controller-mapping-for-device-index          (SDL 2.0.9+)

game-controller-name
game-controller-name-for-index

game-controller-get-axis
game-controller-get-axis-from-string
game-controller-get-axis-from-string-raw
game-controller-get-string-for-axis

game-controller-get-button
game-controller-get-button-from-string
game-controller-get-button-from-string-raw
game-controller-get-string-for-button

game-controller-get-bind-for-axis
game-controller-get-bind-for-button

game-controller-get-player-index                  (SDL 2.0.9+)
game-controller-get-product                       (SDL 2.0.6+)
game-controller-get-product-version               (SDL 2.0.6+)
game-controller-get-vendor                        (SDL 2.0.6+)
game-controller-rumble!                           (SDL 2.0.9+)

game-controller-button-bind?
game-controller-button-bind-type
game-controller-button-bind-type-raw
game-controller-button-bind-button
game-controller-button-bind-axis
game-controller-button-bind-hat
game-controller-button-bind-hat-mask-raw

controller-axis-event-axis-raw
controller-button-event-button-raw
```

### Joystick

New procedures:

```
joystick-get-axis-initial-state                   (SDL 2.0.6+)
joystick-get-device-instance-id                   (SDL 2.0.6+)
joystick-get-device-player-index                  (SDL 2.0.9+)
joystick-get-device-product                       (SDL 2.0.6+)
joystick-get-device-product-version               (SDL 2.0.6+)
joystick-get-device-type                          (SDL 2.0.6+)
joystick-get-device-type-raw                      (SDL 2.0.6+)
joystick-get-device-vendor                        (SDL 2.0.6+)
joystick-get-player-index                         (SDL 2.0.9+)
joystick-get-product                              (SDL 2.0.6+)
joystick-get-product-version                      (SDL 2.0.6+)
joystick-get-type                                 (SDL 2.0.6+)
joystick-get-type-raw                             (SDL 2.0.6+)
joystick-get-vendor                               (SDL 2.0.6+)
joystick-rumble!                                  (SDL 2.0.9+)

lock-joysticks!                                   (SDL 2.0.7+)
unlock-joysticks!                                 (SDL 2.0.7+)
```

### Mouse

New procedures:

```
capture-mouse!                                    (SDL 2.0.4+)
get-global-mouse-state                            (SDL 2.0.4+)
get-global-mouse-state-raw                        (SDL 2.0.4+)
get-mouse-focus
get-mouse-state
get-mouse-state-raw
get-relative-mouse-mode
get-relative-mouse-state
get-relative-mouse-state-raw
relative-mouse-mode-set!
warp-mouse-global!                                (SDL 2.0.4+)
warp-mouse-in-window!
```

### Surface

- You may now pass a pixel format enum symbol (or corresponding
  integer) to `make-surface` and `make-surface*` as the third
  argument, as an alternative to passing a pixel depth (which is also
  still supported).

- You may now pass a pixel format enum symbol (or corresponding
  integer) to `convert-surface` and `convert-surface*` as the second
  argument, as an alternative to passing a `sdl2:pixel-format` struct
  (which is also still supported).

New procedures:

```
create-rgb-surface-with-format*                   (SDL 2.0.5+)
create-rgb-surface-with-format-from*              (SDL 2.0.5+)
duplicate-surface
duplicate-surface*
surface-color-key?
surface-colour-key?
surface-rle?                                      (SDL 2.0.14+)

compose-custom-blend-mode                         (SDL 2.0.6+)

get-yuv-conversion-mode                           (SDL 2.0.8+)
get-yuv-conversion-mode-raw                       (SDL 2.0.8+)
yuv-conversion-mode-set!                          (SDL 2.0.8+)
```

### Vulkan

- Added `vulkan` window flag. (SDL 2.0.6+)

New procedures:

```
vulkan-load-library!                              (SDL 2.0.6+)
vulkan-unload-library!                            (SDL 2.0.6+)
vulkan-get-vk-get-instance-proc-addr              (SDL 2.0.6+)
vulkan-get-instance-extensions                    (SDL 2.0.6+)
vulkan-get-drawable-size                          (SDL 2.0.6+)
vulkan-create-surface                             (SDL 2.0.6+)
```

### Window

- Added `always-on-top` window flag. (SDL 2.0.5+)

New procedures:

```
flash-window!                                     (SDL 2.0.16+)
set-window-modal-for!                             (SDL 2.0.5+)
window-borders-size                               (SDL 2.0.5+)
window-keyboard-grab?                             (SDL 2.0.16+)
window-keyboard-grab-set!                         (SDL 2.0.16+)
window-mouse-grab?                                (SDL 2.0.16+)
window-mouse-grab-set!                            (SDL 2.0.16+)
window-opacity                                    (SDL 2.0.5+)
window-opacity-set!                               (SDL 2.0.5+)
window-resizable?
window-resizable-set!                             (SDL 2.0.5+)
window-always-on-top?                             (SDL 2.0.5+)
window-always-on-top-set!                         (SDL 2.0.16+)
```

### Other Features

- Added hint enums up to SDL 2.0.16.
- Added new blend mode enum.

      mul                                            (SDL 2.0.12+)

- Added new game controller button enums.

      misc1     touchpad                             (SDL 2.0.14+)
      paddle1   paddle2   paddle3   paddle4          (SDL 2.0.14+)

- Added new keyboard keycode enums.

      app1           app2                            (SDL 2.0.5+)
      audio-rewind   audio-fast-forward              (SDL 2.0.5+)

- Added new keyboard scancode enums.

      audio-rewind   audio-fast-forward              (SDL 2.0.5+)

- Added new pixel format enums.

      nv12      nv21                                 (SDL 2.0.4+)
      rgba32    argb32    bgra32    abgr32           (SDL 2.0.5+)

- Added enum groups:
  - Blend factors (SDL 2.0.6+)
  - Blend operations (SDL 2.0.6+)
  - Display event IDs (SDL 2.0.9+)
  - Display orientations (SDL 2.0.9+)
  - Game controller axes
  - Game controller buttons
  - Joystick types (SDL 2.0.6+)
  - Window flash operations (SDL 2.0.16+)

New procedures:

```
get-base-path                                     (SDL 2.0.1+)
get-hint-boolean                                  (SDL 2.0.5+)
get-power-info
get-pref-path                                     (SDL 2.0.1+)
get-touch-device-type                             (SDL 2.0.10+)
get-touch-device-type-raw                         (SDL 2.0.10+)
is-tablet?                                        (SDL 2.0.9+)
open-url                                          (SDL 2.0.16+)
render-integer-scale-set!                         (SDL 2.0.5+)
render-integer-scale?                             (SDL 2.0.5+)
show-simple-message-box
```


## Other Changes

- Added type declarations for over 500 procedures [#13, #47].
  This allows CHICKEN to warn you at compile time if you pass
  incorrect argument types, to make debugging your code easier,
  and allows the compiler to do some performance optimizations.

- Lots of work under the hood to reduce boilerplate for function
  bindings that return an error code on failure. Thanks to megane for
  spearheading this effort.

- Optimized converting symbols to enum integers and vice versa.

- chicken-sdl2 no longer depends on SRFI-18 or SRFI-69.


# 0.3.0 (2019-02-13)

- Ported the egg to be compatible with both CHICKEN 4 and CHICKEN 5.

- Improved the way the installer handles compiler and linker flags [!1].
  It will now attempt to automatically discover the correct compiler and linker flags,
  using the `sdl2-config` command.
  In special cases, set the `SDL2_CFLAGS` and `SDL2_LDFLAGS` environment variables
  (instead of `SDL2_FLAGS`, which is now ignored).
  The installer will prefix the flags with "-C" or "-L" before passing them to `csc`,
  so that they are correctly passed along to the C compiler or linker.
  Thanks to Christian Kellermann for reporting this issue and submitting the initial patch.

- Added support for the SDL2 framework on macOS [#42].
  The installer will automatically use the framework
  if `sdl2-config` is not available and the framework is installed
  in either the `/Library/Frameworks` or `/System/Library/Frameworks` directories.
  Thanks to Laughing Man for suggesting this.
  In special cases you can use the `SDL2_CFLAGS` and `SDL2_LDFLAGS` environment variables:

  ```
  export SDL2_CFLAGS="-F/path/to/your/frameworks"
  export SDL2_LDFLAGS="-F/path/to/your/frameworks -framework SDL2"
  chicken-install sdl2
  ```

- Due to internal changes while porting the egg to CHICKEN 5,
  version feature identifiers such as `sdl2-0.3.0+` and `libSDL-2.0.4+`
  are no longer automatically available at macro-expansion time in compiled code.
  (They are still automatically available at run time with the `feature?` function.)
  If you want to do version detection with `cond-expand` or the `#+foo` syntax,
  you must additionally import the egg for syntax:

  - CHICKEN 4: `(use-for-syntax sdl2)`
  - CHICKEN 5: `(import-for-syntax sdl2)`

- Fixed an issue where managed instances of
  `sdl2:color`, `sdl2:keysym`, `sdl2:point`, and `sdl2:rect`
  would change hash values when the garbage collector runs.
  This caused bugs when using any of those objects as keys in a hash table.
  This is caused by [a quirk in CHICKEN related to locatives](https://bugs.call-cc.org/ticket/1293).
  The quirk has been worked around by no longer storing locatives.

- Fixed a potential security issue with `set-error!`.
  Before, the string was passed directly to `SDL_SetError`,
  which treated the string as a format string (for `vnsprintf`).
  That caused some compilers to emit a security warning: "format string is not a string literal".
  Now, up to 1024 chars of the string are used as verbatim content.

- Fixed incorrect argument type declaration for `render-scale-set!`.
  It accepts floating point numbers, but it was incorrectly declared to accept integers.


# 0.2.0 (2016-02-13)

## Backward Incompatible Changes

There have been several backward incompatible changes to improve type
checks for integer arguments. These changes affect **all function
bindings and struct field setters that accept integer arguments**.
\[[#32](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/32)\]

- All procedures that accept integer arguments now accept inexact
  integers (e.g. 1.0) as well as exact integers (e.g. 1). Before, some
  procedures would signal an exception when given an inexact integer.

- Integer arguments are now checked to make sure they are integers.
  Before, some procedures would accept non-integer numbers (e.g. 1.99)
  and truncate them to integers (e.g. 1), which could cause unexpected
  results. Now, an exception will be signalled if the argument is not
  an integer.

- Integers arguments are now checked to make sure they are in the
  proper range for their data representation. E.g. 8-bit unsigned
  integers must be in the range [0, 255]. Before, integer arguments
  would sometimes overflow, which could cause unexpected results.
  Now, an exception will be signalled if the integer is too low or too
  high.

Other backward incompatible changes in this version:

- `get-window-from-id` now signals an exception of kind `(exn sdl2)`
  if there is no window with the given ID. Before, it returned a null
  sdl2:window, which was inconsistent with this egg's conventions.


## Deprecations

The following procedures have new names, for consistency with Scheme
naming conventions. The old names still exist for backward
compatibility, but will be removed in a future version.
You should migrate to the new names as soon as possible.

- Deprecated: `copy-color`.  Renamed to `color-copy`.
- Deprecated: `copy-colour`. Renamed to `colour-copy`.
- Deprecated: `copy-point`.  Renamed to `point-copy`.
- Deprecated: `copy-rect`.   Renamed to `rect-copy`.

The following procedures are deprecated because they are not very
useful and they are easy to define using other procedures if needed.
They will be removed in a future version.

- Deprecated: `copy-color*`.  Use `(color-copy! c (make-color*))` instead.
- Deprecated: `copy-colour*`. Use `(colour-copy! c (make-colour*))` instead.
- Deprecated: `copy-point*`.  Use `(point-copy! p (make-point*))` instead.
- Deprecated: `copy-rect*`.   Use `(rect-copy! r (make-rect*))` instead.


## Renderer (2D Accelerated Rendering)

Added the **sdl2:renderer** struct record type and many function bindings related to
[2D accelerated rendering](https://wiki.libsdl.org/CategoryRender)
\[[#1](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/1),
[#2](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/2),
[#3](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/3),
[#5](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/5)\]:

```
renderer?
create-renderer!              create-software-renderer!     destroy-renderer!
create-window-and-renderer!   get-renderer
get-renderer-info             get-renderer-info*
num-render-drivers            render-driver-info

renderer-output-size
render-clip-rect              render-clip-rect-set!         render-clip-enabled?
render-logical-size           render-logical-size-set!
render-scale                  render-scale-set!
render-viewport               render-viewport-set!

render-present!
render-copy!                  render-copy-ex!
render-target-supported?      render-target                 render-target-set!
render-read-pixels-raw

render-draw-blend-mode        render-draw-blend-mode-set!   render-draw-blend-mode-raw
render-draw-color             render-draw-color-set!
render-draw-colour            render-draw-colour-set!
render-clear!
render-draw-line!             render-draw-lines!
render-draw-point!            render-draw-points!
render-draw-rect!             render-draw-rects!
render-fill-rect!             render-fill-rects!
```


## Renderer Info

Added the **sdl2:renderer-info** struct record type
\[[#1](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/1)\]:

```
renderer-info?                     free-renderer-info!
renderer-info-name
renderer-info-flags                renderer-info-flags-raw
renderer-info-num-texture-formats
renderer-info-texture-formats      renderer-info-texture-formats-raw
renderer-info-max-texture-width    renderer-info-max-texture-height
```


## Texture

Added the **sdl2:texture** struct record type and many related function bindings
\[[#4](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/4)]:

```
texture?
create-texture                     create-texture*
create-texture-from-surface        create-texture-from-surface*
destroy-texture!
query-texture                      query-texture-raw
texture-format                     texture-access
texture-w                          texture-h
lock-texture-raw!                  unlock-texture!
update-texture-raw!                update-yuv-texture-raw!
texture-alpha-mod                  texture-alpha-mod-set!
texture-blend-mode                 texture-blend-mode-set!
texture-color-mod                  texture-color-mod-set!
texture-colour-mod                 texture-colour-mod-set!
gl-bind-texture!                   gl-unbind-texture!
```


## Hints

Added function bindings related to [hints](https://wiki.libsdl.org/CategoryHints).
\[[#7](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/7)\]

```
get-hint
set-hint!
clear-hints!
```


## SDL 2.0.4 Support

Added support for various feature that were added in SDL 2.0.4. These
are only available if the egg is compiled with SDL 2.0.4 or higher.
\[[#37](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/37)\]

- Added `grabbed-window` procedure.
- Added `point-in-rect?` procedure.
- Added `render-clip-enabled?` procedure.
- Added support for the `mouse-capture` window flag symbol.
- Added `mouse-wheel-event-direction` and
  `mouse-wheel-event-direction-set!` struct field accessors.
- Added support for the `context-release-behavior` OpenGL attribute.
  It can be set to the symbols `none` or `flush`.
- Added `joystick-from-instance-id`.
- Added `joystick-current-power-level`.


## Color, Point, and Rect Operations

- Added `color->values`, `point->values`, and `rect->values`. These
  return the components of those types as multiple values, so that you
  can easily destructure these types with `receive` or `let-values`.

- Renamed several procedures for consistency with Scheme conventions.
  The old names still exist for backward compatibility, but will be
  removed in a future version:

    - `copy-color`  renamed to `color-copy`
    - `copy-colour` renamed to `colour-copy`
    - `copy-point`  renamed to `point-copy`
    - `copy-rect`   renamed to `rect-copy`

- Added `color-copy!` (aka `colour-copy!`), `point-copy!`, and
  `rect-copy!`. They efficiently copy the values from the source
  struct into the destination struct, and return the modified
  destination struct.

- Added various color mathematical operations:

    ```
    color-scale     color-mult     color-add      color-sub
    color-scale!    color-mult!    color-add!     color-sub!
    color-lerp
    color-lerp!

    ("colour" aliases)
    colour-scale    colour-mult    colour-add     colour-sub
    colour-scale!   colour-mult!   colour-add!    colour-sub!
    colour-lerp
    colour-lerp!
    ```

- Added various point mathematical operations:

    ```
    point-scale     point-unscale       point-move
    point-scale!    point-unscale!      point-move!
    point-add       point-sub           point-lerp
    point-add!      point-sub!          point-lerp!
    ```

- Added various rect mathematical operations:

    ```
    rect-scale      rect-unscale
    rect-scale!     rect-unscale!
    rect-move       rect-add-point      rect-sub-point
    rect-move!      rect-add-point!     rect-sub-point!
    rect-grow       rect-grow/center
    rect-grow!      rect-grow/center!
    rect-lerp       rect-lerp-xy
    rect-lerp!      rect-lerp-xy!
    ```


## Performance Improvements

- Managed instances of sdl2:color, sdl2:event, sdl2:keysym,
  sdl2:point, and sdl2:rect are now allocated in CHICKEN-managed
  memory. This improves performance of garbage collecting managed
  instances of those types. Unmanaged instances are not affected.
  Thanks to Kooda Loutre for suggesting this improvement.
  \[[#30](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/30)\]

- Significantly improved the performance of initializing managed and
  unmanaged instances of sdl2:color, sdl2:point, and sdl2:rect.

- Significantly improved performance of `color-set!`, `point-set!`,
  and `rect-set!` when all arguments are specified. This performance
  improvement does not apply if any argument is omitted or #f.

- Fixed a performance bug in all struct field setters. Field guards
  were being created every time a field setter was called. Each guard
  is now created only once, at startup, as was originally intended.
  This improves the performance of all struct field setters.


## Feature Identifiers

Added feature identifiers which can be used to detect the versions of
the sdl2 egg and SDL at compile time or run time, for example using
the `cond-expand` macro, `#+foo` syntax, or the `feature?` procedure.

Some or all of the following feature identifiers will be registered
depending on the circumstances:

```
sdl2                Using any version of the sdl2 egg
sdl2-0.2.0+         Using sdl2 egg 0.2.0 or higher

libSDL-2.0.0+       sdl2 egg was compiled with SDL 2.0.0 or higher
libSDL-2.0.1+       ... SDL 2.0.1 or higher
libSDL-2.0.2+       ... SDL 2.0.2 or higher
libSDL-2.0.3+       ... SDL 2.0.3 or higher
libSDL-2.0.4+       ... SDL 2.0.4 or higher
```

These are cumulative, so if you are using sdl2 egg version 0.2.0 with
SDL version 2.0.4, all of the above identifiers will be defined.


## Other changes

- Added the `struct-eq?` procedure. You should use this procedure to
  compare the identities of struct instances. Other procedures, such
  as `eq?` or `equal?`, may not work correctly in the future due to
  changes in the implementation of struct records.

- Added the `egg-version` procedure, which returns a list of three
  integers indicating the version of the sdl2 egg itself. This is
  independent of the version of SDL.

- `fill-rects!` and `update-window-surface-rects!` can now accept a
  vector of sdl2:rects as well as a list of sdl2:rects. Before, they
  only accepted a list of sdl2:rects.

- `enclose-points` now can now accept a vector of sdl2:points as well
  as a list of sdl2:points. Before, it only accepted a list of
  sdl2:points.

- Added support for the `allow-high-dpi` window flag symbol.
  It requires SDL version 2.0.1 or higher.

- Added an install flag to enable profiling in the sdl2 egg. If you
  run chicken-install with the `-D sdl2-profile` flag, the sdl2 egg
  will be compiled with the `-profile` flag, which enables performance
  profiling of sdl2 procedures when any program uses the sdl2 egg.
  This can help you diagnose performance issues in the sdl2 egg and/or
  in your program. See the CHICKEN manual section
  "[Using the compiler](https://wiki.call-cc.org/man/5/Using%20the%20compiler)"
  for more information about profiling. Profiling greatly reduces
  performance, and creates a new "PROFILE.###" file each time the
  program is run, so you should only enable profiling when you are
  diagnosing performance issues.



# 0.1.1 (2015-12-22)

- Fixed a compile error when compiling with GCC 4:
  "‘for’ loop initial declarations are only allowed in C99 mode".
  Thanks to Evan Hanson for reporting this issue.
  \[[#29](https://gitlab.com/chicken-sdl2/chicken-sdl2/issues/29)\]



# 0.1.0 (2015-12-19)

Initial release. The following procedures were included:

## General

Initialization and cleanup:

```
set-main-ready!
init!                              init-subsystem!
quit!                              quit-subsystem!
was-init
```

Miscellaneous:

```
get-error                          set-error!                         clear-error!
get-platform
screen-saver-enabled?              screen-saver-enabled-set!
has-clipboard-text?                get-clipboard-text                 set-clipboard-text!
```

Struct memory management:

```
struct-null?
```


## Color

Related to the **sdl2:color** struct record type
(each procedure has an alias spelled as "colour"):

```
color?                   colour?
make-color               make-color*              free-color!
make-colour              make-colour*             free-colour!
color-r                  color-r-set!             colour-r                 colour-r-set!
color-g                  color-g-set!             colour-g                 colour-g-set!
color-b                  color-b-set!             colour-b                 colour-b-set!
color-a                  color-a-set!             colour-a                 colour-a-set!
color-set!                                        colour-set!
color->list                                       colour->list
color=?                                           colour=?
copy-color                                        copy-colour
copy-color*                                       copy-colour*
```


## Event

```
event-state                        event-state-set!
flush-event!                       flush-events!
has-event?                         has-events?                        quit-requested?
get-events!                        peek-events                        poll-event!
pump-events!                       push-event!
wait-event!                        wait-event-timeout!
register-events!
```

Related to the **sdl2:event** struct record type:

```
event?
make-event                         make-event*                        free-event!
event-type                         event-type-set!                    event-type-raw
event-timestamp                    event-timestamp-set!
```

Related to the **sdl2:controller-axis-event** variant:

```
controller-axis-event?
controller-axis-event-which        controller-axis-event-which-set!
controller-axis-event-axis         controller-axis-event-axis-set!
controller-axis-event-value        controller-axis-event-value-set!
```

Related to the **sdl2:controller-button-event** variant:

```
controller-button-event?
controller-button-event-which      controller-button-event-which-set!
controller-button-event-button     controller-button-event-button-set!
controller-button-event-state      controller-button-event-state-set!
```

Related to the **sdl2:controller-device-event** variant:

```
controller-device-event?
controller-device-event-which      controller-device-event-which-set!
```

Related to the **sdl2:dollar-gesture-event** variant:

```
dollar-gesture-event?
dollar-gesture-event-touch-id      dollar-gesture-event-touch-id-set!
dollar-gesture-event-gesture-id    dollar-gesture-event-gesture-id-set!
dollar-gesture-event-num-fingers   dollar-gesture-event-num-fingers-set!
dollar-gesture-event-error         dollar-gesture-event-error-set!
dollar-gesture-event-x             dollar-gesture-event-x-set!
dollar-gesture-event-y             dollar-gesture-event-y-set!
```

Related to the **sdl2:drop-event** variant:

```
drop-event?
drop-event-file                    drop-event-file-set!
```

Related to the **sdl2:joy-axis-event** variant:

```
joy-axis-event?
joy-axis-event-which               joy-axis-event-which-set!
joy-axis-event-axis                joy-axis-event-axis-set!
joy-axis-event-value               joy-axis-event-value-set!
```

Related to the **sdl2:joy-ball-event** variant:

```
joy-ball-event?
joy-ball-event-which               joy-ball-event-which-set!
joy-ball-event-ball                joy-ball-event-ball-set!
joy-ball-event-xrel                joy-ball-event-xrel-set!
joy-ball-event-yrel                joy-ball-event-yrel-set!
```

Related to the **sdl2:joy-button-event** variant:

```
joy-button-event?
joy-button-event-which             joy-button-event-which-set!
joy-button-event-button            joy-button-event-button-set!
joy-button-event-state             joy-button-event-state-set!
```

Related to the **sdl2:joy-device-event** variant:

```
joy-device-event?
joy-device-event-which             joy-device-event-which-set!
```

Related to the **sdl2:joy-hat-event** variant:

```
joy-hat-event?
joy-hat-event-which                joy-hat-event-which-set!
joy-hat-event-hat                  joy-hat-event-hat-set!
joy-hat-event-value                joy-hat-event-value-set!           joy-hat-event-value-raw
```

Related to the **sdl2:keyboard-event** variant:

```
keyboard-event?
keyboard-event-window-id           keyboard-event-window-id-set!
keyboard-event-state               keyboard-event-state-set!
keyboard-event-repeat              keyboard-event-repeat-set!
keyboard-event-keysym              keyboard-event-keysym-set!
keyboard-event-scancode            keyboard-event-scancode-set!       keyboard-event-scancode-raw
keyboard-event-sym                 keyboard-event-sym-set!            keyboard-event-sym-raw
keyboard-event-mod                 keyboard-event-mod-set!            keyboard-event-mod-raw
```

Related to the **sdl2:mouse-button-event** variant:

```
mouse-button-event?
mouse-button-event-window-id       mouse-button-event-window-id-set!
mouse-button-event-which           mouse-button-event-which-set!
mouse-button-event-button          mouse-button-event-button-set!     mouse-button-event-button-raw
mouse-button-event-state           mouse-button-event-state-set!
mouse-button-event-x               mouse-button-event-x-set!
mouse-button-event-y               mouse-button-event-y-set!
```

Related to the **sdl2:mouse-motion-event** variant:

```
mouse-motion-event?
mouse-motion-event-window-id       mouse-motion-event-window-id-set!
mouse-motion-event-which           mouse-motion-event-which-set!
mouse-motion-event-state           mouse-motion-event-state-set!      mouse-motion-event-state-raw
mouse-motion-event-x               mouse-motion-event-x-set!
mouse-motion-event-y               mouse-motion-event-y-set!
mouse-motion-event-xrel            mouse-motion-event-xrel-set!
mouse-motion-event-yrel            mouse-motion-event-yrel-set!
```

Related to the **sdl2:mouse-wheel-event** variant:

```
mouse-wheel-event?
mouse-wheel-event-window-id        mouse-wheel-event-window-id-set!
mouse-wheel-event-which            mouse-wheel-event-which-set!
mouse-wheel-event-x                mouse-wheel-event-x-set!
mouse-wheel-event-y                mouse-wheel-event-y-set!
```

Related to the **sdl2:multi-gesture-event** variant:

```
multi-gesture-event?
multi-gesture-event-touch-id       multi-gesture-event-touch-id-set!
multi-gesture-event-dtheta         multi-gesture-event-dtheta-set!
multi-gesture-event-ddist          multi-gesture-event-ddist-set!
multi-gesture-event-x              multi-gesture-event-x-set!
multi-gesture-event-y              multi-gesture-event-y-set!
multi-gesture-event-num-fingers    multi-gesture-event-num-fingers-set!
```

Related to the **sdl2:quit-event** variant:

```
quit-event?
```

Related to the **sdl2:sys-wm-event** variant:

```
sys-wm-event?
sys-wm-event-msg-raw               sys-wm-event-msg-raw-set!
```

Related to the **sdl2:text-editing-event** variant:

```
text-editing-event?
text-editing-event-window-id       text-editing-event-window-id-set!
text-editing-event-text            text-editing-event-text-set!
text-editing-event-start           text-editing-event-start-set!
text-editing-event-length          text-editing-event-length-set!
```

Related to the **sdl2:text-input-event** variant:

```
text-input-event?
text-input-event-window-id         text-input-event-window-id-set!
text-input-event-text              text-input-event-text-set!
```

Related to the **sdl2:touch-finger-event** variant:

```
touch-finger-event?
touch-finger-event-touch-id        touch-finger-event-touch-id-set!
touch-finger-event-finger-id       touch-finger-event-finger-id-set!
touch-finger-event-x               touch-finger-event-x-set!
touch-finger-event-y               touch-finger-event-y-set!
touch-finger-event-dx              touch-finger-event-dx-set!
touch-finger-event-dy              touch-finger-event-dy-set!
touch-finger-event-pressure        touch-finger-event-pressure-set!
```

Related to the **sdl2:user-event** variant:

```
user-event?
user-event-window-id               user-event-window-id-set!
user-event-code                    user-event-code-set!
user-event-data1-raw               user-event-data1-raw-set!
user-event-data2-raw               user-event-data2-raw-set!
```

Related to the **sdl2:window-event** variant:

```
window-event?
window-event-window-id             window-event-window-id-set!
window-event-event                 window-event-event-set!            window-event-event-raw
window-event-data1                 window-event-data1-set!
window-event-data2                 window-event-data2-set!
```


## Joystick

```
num-joysticks                      joystick-open!                     joystick-close!
joystick-update!                   joystick-event-state               joystick-event-state-set!
joystick-name-for-index
joystick-get-device-guid           joystick-get-guid-from-string
```

Related to the **sdl2:joystick** struct record type:

```
joystick?
joystick-attached?
joystick-name
joystick-num-axes                  joystick-get-axis
joystick-num-balls                 joystick-get-ball
joystick-num-buttons               joystick-get-button
joystick-num-hats                  joystick-get-hat                   joystick-get-hat-raw
joystick-instance-id               joystick-get-guid
```

Related to the **sdl2:joystick-guid** struct record type:

```
joystick-guid?                     free-joystick-guid!
joystick-get-guid-string
```


## Keyboard

```
get-key-from-name                  get-key-from-name-raw
get-key-from-scancode              get-key-from-scancode-raw
get-key-name
get-scancode-from-name             get-scancode-from-name-raw
get-scancode-from-key              get-scancode-from-key-raw
get-scancode-name
get-keyboard-focus
scancode-pressed?
mod-state                          mod-state-set!                     mod-state-raw
text-input-rect-set!
start-text-input!                  stop-text-input!                   text-input-active?
screen-keyboard-support?           screen-keyboard-shown?
```

Related to the **sdl2:keysym** struct record type:

```
keysym?
make-keysym                        make-keysym*                       free-keysym!
keysym-scancode                    keysym-scancode-set!               keysym-scancode-raw
keysym-sym                         keysym-sym-set!                    keysym-sym-raw
keysym-mod                         keysym-mod-set!                    keysym-mod-raw
```


## OpenGL integration

```
gl-create-context!                 gl-delete-context!                 gl-make-current!
gl-get-current-window              gl-get-current-context
gl-attribute                       gl-attribute-set!                  gl-reset-attributes!
gl-get-drawable-size
gl-swap-window!                    gl-swap-interval                   gl-set-swap-interval!
gl-extension-supported?
```

Related to the **sdl2:gl-context** struct record type:

```
gl-context?
```


## Palette

Related to the **sdl2:palette** struct record type:

```
palette?
make-palette                       make-palette*                      free-palette!
palette-ncolors                    palette-ncolours
palette-ref                        palette-set!
palette-colors                     palette-colours
palette-colors-set!                palette-colours-set!
```


## Pixel Format

```
map-rgb                            map-rgba
get-rgb                            get-rgba
pixel-format-enum-to-masks
```

Related to the **sdl2:pixel-format** struct record type:

```
pixel-format?
make-pixel-format                  make-pixel-format*                 free-pixel-format!
pixel-format-format                pixel-format-format-raw
pixel-format-palette               pixel-format-palette-set!
pixel-format-bits-per-pixel
pixel-format-bytes-per-pixel
pixel-format-rmask
pixel-format-gmask
pixel-format-bmask
pixel-format-amask
```


## Rect / Point

```
rect-empty?                        enclose-points                     has-intersection?
intersect-rect                     intersect-rect-and-line            union-rect
```

Related to the **sdl2:rect** struct record type:

```
rect?
make-rect                          make-rect*                         free-rect!
rect-x                             rect-x-set!
rect-y                             rect-y-set!
rect-w                             rect-w-set!
rect-h                             rect-h-set!
rect-set!                          rect->list                         rect=?
copy-rect                          copy-rect*
```

Related to the **sdl2:point** struct record type:

```
point?
make-point                         make-point*                        free-point!
point-x                            point-x-set!
point-y                            point-y-set!
point-set!                         point->list                        point=?
copy-point                         copy-point*
```


## RWops

```
rw-from-file                       rw-from-const-mem                  rw-from-mem
rw-from-blob                       rw-from-string
rw-close!
```

Related to the **sdl2:rwops** struct record type:

```
rwops?
rwops-type                         rwops-type-raw
```


## Surface

```
create-rgb-surface*                create-rgb-surface-from*           convert-surface
load-bmp                           load-bmp*
load-bmp-rw                        load-bmp-rw*
save-bmp!                          save-bmp-rw!
lock-surface!                      unlock-surface!                    must-lock?
blit-surface!                      blit-scaled!
fill-rect!                         fill-rects!
rotate-surface-90                  rotate-surface-90*
flip-surface                       flip-surface*
```

Related to the **sdl2:surface** struct record type:

```
surface?
make-surface                       make-surface*                      free-surface!
surface-format
surface-w
surface-h
surface-pitch
surface-pixels-raw
surface-userdata-raw               surface-userdata-raw-set!
surface-refcount                   surface-refcount-set!
surface-ref                        surface-set!                       surface-ref-raw
surface-clip-rect                  surface-clip-rect-set!
surface-color-key                  surface-color-key-set!             surface-color-key-raw
surface-colour-key                 surface-colour-key-set!            surface-colour-key-raw
surface-alpha-mod                  surface-alpha-mod-set!
surface-blend-mode                 surface-blend-mode-set!            surface-blend-mode-raw
surface-color-mod                  surface-colour-mod
surface-color-mod-set!             surface-colour-mod-set!
surface-palette                    surface-palette-set!
surface-rle-set!
```


## Timer

```
delay!                             get-ticks
get-performance-counter            get-performance-frequency
```


## Touch / Gesture

```
get-num-touch-devices              get-touch-device
get-num-touch-fingers              get-touch-finger
```

Related to the **sdl2:finger** struct record type:

```
finger?
finger-id
finger-x                           finger-y
finger-pressure
```


## Version

```
version-at-least?                  compiled-version                   current-version
```


## Window / Display Mode

```
create-window!                     destroy-window!                    get-window-from-id
update-window-surface!             update-window-surface-rects!
show-window!                       hide-window!                       raise-window!
maximize-window!                   minimize-window!                   restore-window!
```

Related to the **sdl2:window** struct record type:

```
window?
window-id
window-bordered?                   window-bordered-set!
window-brightness                  window-brightness-set!
window-display-index
window-display-mode                window-display-mode-set!
window-flags                       window-flags-raw
window-fullscreen                  window-fullscreen-set!
window-grab?                       window-grab-set!
window-icon-set!
window-maximum-size                window-maximum-size-set!
window-minimum-size                window-minimum-size-set!
window-pixel-format                window-pixel-format-raw
window-position                    window-position-set!
window-size                        window-size-set!
window-surface
window-title                       window-title-set!
```

Related to the **sdl2:display-mode** struct record type:

```
display-mode?
make-display-mode                  make-display-mode*                 free-display-mode!
display-mode-format                display-mode-format-set!           display-mode-format-raw
display-mode-w                     display-mode-w-set!
display-mode-h                     display-mode-h-set!
display-mode-refresh-rate          display-mode-refresh-rate-set!
```
