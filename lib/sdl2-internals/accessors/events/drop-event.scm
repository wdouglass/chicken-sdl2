;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export drop-event?
        drop-event-file
        drop-event-file-set!
        drop-event-window-id ;; SDL 2.0.5+
        drop-event-window-id-set! ;; SDL 2.0.5+
        )


;;; NOTE: Public type declarations are in shared/reexport-types.scm

(cond-expand
 (libSDL-2.0.5+
  (define-event-type "SDL_DropEvent"
    types: (SDL_DROPFILE
            SDL_DROPTEXT
            SDL_DROPBEGIN
            SDL_DROPCOMPLETE)
    pred:  drop-event?
    print: ((file drop-event-file))
    ("drop.file"
     type:   c-string
     getter: drop-event-file
     setter: drop-event-file-set!)
    ("drop.windowID"
     type:   Uint32
     getter: drop-event-window-id
     setter: drop-event-window-id-set!
     guard:  (Uint32-guard "sdl2:drop-event field windowID"))))
 (else
  (define-event-type "SDL_DropEvent"
    types: (SDL_DROPFILE)
    pred:  drop-event?
    print: ((file drop-event-file))
    ("drop.file"
     type:   c-string
     getter: drop-event-file
     setter: drop-event-file-set!))

  (define-versioned (drop-event-window-id) libSDL-2.0.5+ (void))
  (define-versioned (drop-event-window-id-set!) libSDL-2.0.5+ (void))))
