;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export game-controller-button-bind-type
        game-controller-button-bind-type-raw
        game-controller-button-bind-button
        game-controller-button-bind-axis
        game-controller-button-bind-hat
        game-controller-button-bind-hat-mask-raw)


(define-struct-field-accessors
  SDL_GameControllerButtonBind*
  game-controller-button-bind?
  ("bindType"
   type:   SDL_GameControllerBindType
   getter: game-controller-button-bind-type-raw)
  ("value.button"
   type:   Sint32
   getter: %game-controller-button-bind-button)
  ("value.axis"
   type:   Sint32
   getter: %game-controller-button-bind-axis)
  ("value.hat.hat"
   type:   Sint32
   getter: %game-controller-button-bind-hat)
  ("value.hat.hat_mask"
   type:   Sint32
   getter: %game-controller-button-bind-hat-mask-raw))


(define-enum-accessor
  getter: (game-controller-button-bind-type
           raw:  game-controller-button-bind-type-raw
           conv: game-controller-bind-type->symbol))

(define (game-controller-button-bind-button bind)
  (if (and (game-controller-button-bind? bind)
           (= SDL_CONTROLLER_BINDTYPE_BUTTON
              (game-controller-button-bind-type-raw bind)))
      (%game-controller-button-bind-button bind)
      (error 'game-controller-button-bind-button
             "not a sdl2:game-controller-button-bind with type button"
             bind)))

(define (game-controller-button-bind-axis bind)
  (if (and (game-controller-button-bind? bind)
           (= SDL_CONTROLLER_BINDTYPE_AXIS
              (game-controller-button-bind-type-raw bind)))
      (%game-controller-button-bind-axis bind)
      (error 'game-controller-button-bind-axis
             "not a sdl2:game-controller-button-bind with type axis"
             bind)))

(define (game-controller-button-bind-hat bind)
  (if (and (game-controller-button-bind? bind)
           (= SDL_CONTROLLER_BINDTYPE_HAT
              (game-controller-button-bind-type-raw bind)))
      (%game-controller-button-bind-hat bind)
      (error 'game-controller-button-bind-hat
             "not a sdl2:game-controller-button-bind with type hat"
             bind)))

(define (game-controller-button-bind-hat-mask-raw bind)
  (if (and (game-controller-button-bind? bind)
           (= SDL_CONTROLLER_BINDTYPE_HAT
              (game-controller-button-bind-type-raw bind)))
      (%game-controller-button-bind-hat-mask-raw bind)
      (error 'game-controller-button-bind-hat-mask-raw
             "not a sdl2:game-controller-button-bind with type hat"
             bind)))
