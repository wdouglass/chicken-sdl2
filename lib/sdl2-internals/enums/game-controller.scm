;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export symbol->game-controller-axis
        game-controller-axis->symbol

        symbol->game-controller-button
        game-controller-button->symbol

        symbol->game-controller-bind-type
        game-controller-bind-type->symbol)


(define-enum-mappings
  type: SDL_GameControllerAxis
  symbol->value: symbol->game-controller-axis
  value->symbol: game-controller-axis->symbol

  ((invalid        SDL_CONTROLLER_AXIS_INVALID)
   (left-x         SDL_CONTROLLER_AXIS_LEFTX)
   (left-y         SDL_CONTROLLER_AXIS_LEFTY)
   (right-x        SDL_CONTROLLER_AXIS_RIGHTX)
   (right-y        SDL_CONTROLLER_AXIS_RIGHTY)
   (trigger-left   SDL_CONTROLLER_AXIS_TRIGGERLEFT)
   (trigger-right  SDL_CONTROLLER_AXIS_TRIGGERRIGHT)
   (max            SDL_CONTROLLER_AXIS_MAX)))


(define-enum-mappings
  type: SDL_GameControllerButton
  symbol->value: symbol->game-controller-button
  value->symbol: game-controller-button->symbol

  ((required: libSDL-2.0.0+ "SDL_VERSION_ATLEAST(2,0,0)")
   (invalid         SDL_CONTROLLER_BUTTON_INVALID)
   (a               SDL_CONTROLLER_BUTTON_A)
   (b               SDL_CONTROLLER_BUTTON_B)
   (x               SDL_CONTROLLER_BUTTON_X)
   (y               SDL_CONTROLLER_BUTTON_Y)
   (back            SDL_CONTROLLER_BUTTON_BACK)
   (guide           SDL_CONTROLLER_BUTTON_GUIDE)
   (start           SDL_CONTROLLER_BUTTON_START)
   (left-stick      SDL_CONTROLLER_BUTTON_LEFTSTICK)
   (right-stick     SDL_CONTROLLER_BUTTON_RIGHTSTICK)
   (left-shoulder   SDL_CONTROLLER_BUTTON_LEFTSHOULDER)
   (right-shoulder  SDL_CONTROLLER_BUTTON_RIGHTSHOULDER)
   (dpad-up         SDL_CONTROLLER_BUTTON_DPAD_UP)
   (dpad-down       SDL_CONTROLLER_BUTTON_DPAD_DOWN)
   (dpad-left       SDL_CONTROLLER_BUTTON_DPAD_LEFT)
   (dpad-right      SDL_CONTROLLER_BUTTON_DPAD_RIGHT)
   (max             SDL_CONTROLLER_BUTTON_MAX))

  ((required: libSDL-2.0.14+ "SDL_VERSION_ATLEAST(2,0,14)")
   (misc1           SDL_CONTROLLER_BUTTON_MISC1)
   (paddle1         SDL_CONTROLLER_BUTTON_PADDLE1)
   (paddle2         SDL_CONTROLLER_BUTTON_PADDLE2)
   (paddle3         SDL_CONTROLLER_BUTTON_PADDLE3)
   (paddle4         SDL_CONTROLLER_BUTTON_PADDLE4)
   (touchpad        SDL_CONTROLLER_BUTTON_TOUCHPAD)))


(define-enum-mappings
  type: SDL_GameControllerBindType
  symbol->value: symbol->game-controller-bind-type
  value->symbol: game-controller-bind-type->symbol

  ((none    SDL_CONTROLLER_BINDTYPE_NONE)
   (button  SDL_CONTROLLER_BINDTYPE_BUTTON)
   (axis    SDL_CONTROLLER_BINDTYPE_AXIS)
   (hat     SDL_CONTROLLER_BINDTYPE_HAT)))
