;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GENERAL

(define-foreign-constants int
  SDL_ENABLE
  SDL_DISABLE
  SDL_QUERY
  SDL_IGNORE

  SDL_BYTEORDER
  SDL_BIG_ENDIAN
  SDL_LIL_ENDIAN)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INIT / SUBSYSTEMS

(export symbol->init-flag
        init-flag->symbol
        pack-init-flags
        unpack-init-flags)

(define-enum-mappings
  type: Uint32
  symbol->value: symbol->init-flag
  value->symbol: init-flag->symbol

  ((timer            SDL_INIT_TIMER)
   (audio            SDL_INIT_AUDIO)
   (video            SDL_INIT_VIDEO)
   (joystick         SDL_INIT_JOYSTICK)
   (haptic           SDL_INIT_HAPTIC)
   (game-controller  SDL_INIT_GAMECONTROLLER)
   (events           SDL_INIT_EVENTS)
   (everything       SDL_INIT_EVERYTHING)))

(define-enum-mask-packer pack-init-flags
  symbol->init-flag)

(define-enum-mask-unpacker unpack-init-flags
  init-flag->symbol
  (list SDL_INIT_TIMER
        SDL_INIT_AUDIO
        SDL_INIT_VIDEO
        SDL_INIT_JOYSTICK
        SDL_INIT_HAPTIC
        SDL_INIT_GAMECONTROLLER
        SDL_INIT_EVENTS
        SDL_INIT_EVERYTHING))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MESSAGE BOX

(export symbol->message-box-flag
        message-box-flag->symbol)

(define-enum-mappings
  type: SDL_MessageBoxFlags
  symbol->value: symbol->message-box-flag
  value->symbol: message-box-flag->symbol

  ((error        SDL_MESSAGEBOX_ERROR)
   (warning      SDL_MESSAGEBOX_WARNING)
   (information  SDL_MESSAGEBOX_INFORMATION)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; POWER STATE

(export symbol->power-state
        power-state->symbol)

(define-enum-mappings
  type: SDL_PowerState
  symbol->value: symbol->power-state
  value->symbol: power-state->symbol

  ((unknown     SDL_POWERSTATE_UNKNOWN)
   (on-battery  SDL_POWERSTATE_ON_BATTERY)
   (no-battery  SDL_POWERSTATE_NO_BATTERY)
   (charging    SDL_POWERSTATE_CHARGING)
   (charged     SDL_POWERSTATE_CHARGED)))
