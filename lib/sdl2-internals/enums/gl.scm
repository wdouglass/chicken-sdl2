;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GL ATTR

(export symbol->gl-attr
        gl-attr->symbol)


(define-enum-mappings
  type: SDL_GLattr
  symbol->value: symbol->gl-attr
  value->symbol: gl-attr->symbol

  ((required: libSDL-2.0.0+ "SDL_VERSION_ATLEAST(2,0,0)")
   (red-size                    SDL_GL_RED_SIZE)
   (green-size                  SDL_GL_GREEN_SIZE)
   (blue-size                   SDL_GL_BLUE_SIZE)
   (alpha-size                  SDL_GL_ALPHA_SIZE)
   (buffer-size                 SDL_GL_BUFFER_SIZE)
   (doublebuffer                SDL_GL_DOUBLEBUFFER)
   (depth-size                  SDL_GL_DEPTH_SIZE)
   (stencil-size                SDL_GL_STENCIL_SIZE)
   (accum-red-size              SDL_GL_ACCUM_RED_SIZE)
   (accum-green-size            SDL_GL_ACCUM_GREEN_SIZE)
   (accum-blue-size             SDL_GL_ACCUM_BLUE_SIZE)
   (accum-alpha-size            SDL_GL_ACCUM_ALPHA_SIZE)
   (stereo                      SDL_GL_STEREO)
   (multisamplebuffers          SDL_GL_MULTISAMPLEBUFFERS)
   (multisamplesamples          SDL_GL_MULTISAMPLESAMPLES)
   (accelerated-visual          SDL_GL_ACCELERATED_VISUAL)
   ;; omitted:                  SDL_GL_RETAINED_BACKING (deprecated)
   (context-major-version       SDL_GL_CONTEXT_MAJOR_VERSION)
   (context-minor-version       SDL_GL_CONTEXT_MINOR_VERSION)
   ;; omitted:                  SDL_GL_CONTEXT_EGL (deprecated)
   (context-flags               SDL_GL_CONTEXT_FLAGS)
   (context-profile-mask        SDL_GL_CONTEXT_PROFILE_MASK)
   (share-with-current-context  SDL_GL_SHARE_WITH_CURRENT_CONTEXT))

  ((required: libSDL-2.0.1+ "SDL_VERSION_ATLEAST(2,0,1)")
   (framebuffer-srgb-capable    SDL_GL_FRAMEBUFFER_SRGB_CAPABLE))

  ((required: libSDL-2.0.4+ "SDL_VERSION_ATLEAST(2,0,4)")
   (context-release-behavior    SDL_GL_CONTEXT_RELEASE_BEHAVIOR)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GL PROFILE

(export symbol->gl-profile
        gl-profile->symbol)


(define-enum-mappings
  type: SDL_GLprofile
  symbol->value: symbol->gl-profile
  value->symbol: gl-profile->symbol

  ((core           SDL_GL_CONTEXT_PROFILE_CORE)
   (compatibility  SDL_GL_CONTEXT_PROFILE_COMPATIBILITY)
   (es             SDL_GL_CONTEXT_PROFILE_ES)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GL CONTEXT FLAG

(export symbol->gl-context-flag
        gl-context-flag->symbol
        pack-gl-context-flags
        unpack-gl-context-flags)


(define-enum-mappings
  type: SDL_GLcontextFlag
  symbol->value: symbol->gl-context-flag
  value->symbol: gl-context-flag->symbol

  ((debug               SDL_GL_CONTEXT_DEBUG_FLAG)
   (forward-compatible  SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG)
   (robust-access       SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG)
   (reset-isolation     SDL_GL_CONTEXT_RESET_ISOLATION_FLAG)))


(define-enum-mask-packer pack-gl-context-flags
  symbol->gl-context-flag)


(define-enum-mask-unpacker unpack-gl-context-flags
  gl-context-flag->symbol
  (list SDL_GL_CONTEXT_DEBUG_FLAG
        SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG
        SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG
        SDL_GL_CONTEXT_RESET_ISOLATION_FLAG))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GL CONTEXT RELEASE FLAG

#+libSDL-2.0.4+
(begin
  (export symbol->gl-context-release-flag
          gl-context-release-flag->symbol)

  (define-enum-mappings
    type: SDL_GLcontextReleaseFlag
    symbol->value: symbol->gl-context-release-flag
    value->symbol: gl-context-release-flag->symbol

    ((none   SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE)
     (flush  SDL_GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH))))
