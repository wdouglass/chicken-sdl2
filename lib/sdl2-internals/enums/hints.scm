;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export symbol->hint
        hints-alist)


(define-foreign-constants SDL_HintPriority
  SDL_HINT_DEFAULT
  SDL_HINT_NORMAL
  SDL_HINT_OVERRIDE)


;;; Hints are defined differently than most other enums in this
;;; library. Instead of using the value from SDL's headers at compile
;;; time, the values are hardcoded into this library. The reasons for
;;; doing it this way are:
;;;
;;; - So that developers do not need to check the compiled version of
;;;   SDL before setting a hint. Developers can safely set any hint
;;;   with any version of SDL (unrecognized hints are ignored).
;;;
;;; - Because the constant values are explicitly defined strings, not
;;;   enums which have implicit values determined at compile time.
;;;
;;; - Because new hints are often added in new SDL versions, and the
;;;   implementation would be much more complex if we needed to check
;;;   the SDL version for every constant.

(define (symbol->hint symbol #!optional not-found-callback)
  (or (alist-ref symbol hints-alist)
      (if not-found-callback
          (not-found-callback symbol)
          (error "Unrecognized hint name" symbol))))

(define hints-alist
  '(
    ;; Effective in SDL 2.0.0 and later
    (framebuffer-acceleration                     . "SDL_FRAMEBUFFER_ACCELERATION")
    (gamecontrollerconfig                         . "SDL_GAMECONTROLLERCONFIG")
    (grab-keyboard                                . "SDL_GRAB_KEYBOARD")
    (idle-timer-disabled                          . "SDL_IOS_IDLE_TIMER_DISABLED") ; irregular
    (joystick-allow-background-events             . "SDL_JOYSTICK_ALLOW_BACKGROUND_EVENTS")
    (orientations                                 . "SDL_IOS_ORIENTATIONS") ; irregular
    (render-driver                                . "SDL_RENDER_DRIVER")
    (render-opengl-shaders                        . "SDL_RENDER_OPENGL_SHADERS")
    (render-scale-quality                         . "SDL_RENDER_SCALE_QUALITY")
    (render-vsync                                 . "SDL_RENDER_VSYNC")
    (timer-resolution                             . "SDL_TIMER_RESOLUTION")
    (video-minimize-on-focus-loss                 . "SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS")
    (video-x11-xinerama                           . "SDL_VIDEO_X11_XINERAMA")
    (video-x11-xrandr                             . "SDL_VIDEO_X11_XRANDR")
    (video-x11-xvidmode                           . "SDL_VIDEO_X11_XVIDMODE")
    (xinput-enabled                               . "SDL_XINPUT_ENABLED")

    ;; Effective in SDL 2.0.1 and later
    (render-direct3d-threadsafe                   . "SDL_RENDER_DIRECT3D_THREADSAFE")
    (video-highdpi-disabled                       . "SDL_VIDEO_HIGHDPI_DISABLED")

    ;; Effective in SDL 2.0.2 and later
    (accelerometer-as-joystick                    . "SDL_ACCELEROMETER_AS_JOYSTICK")
    (mac-ctrl-click-emulate-right-click           . "SDL_MAC_CTRL_CLICK_EMULATE_RIGHT_CLICK")
    (mouse-relative-mode-warp                     . "SDL_MOUSE_RELATIVE_MODE_WARP")
    (video-allow-screensaver                      . "SDL_VIDEO_ALLOW_SCREENSAVER")
    (video-mac-fullscreen-spaces                  . "SDL_VIDEO_MAC_FULLSCREEN_SPACES")
    (video-window-share-pixel-format              . "SDL_VIDEO_WINDOW_SHARE_PIXEL_FORMAT")
    (video-win-d3dcompiler                        . "SDL_VIDEO_WIN_D3DCOMPILER")

    ;; Effective in SDL 2.0.3 and later
    (render-direct3d11-debug                      . "SDL_RENDER_DIRECT3D11_DEBUG")
    (winrt-handle-back-button                     . "SDL_WINRT_HANDLE_BACK_BUTTON")
    (winrt-privacy-policy-label                   . "SDL_WINRT_PRIVACY_POLICY_LABEL")
    (winrt-privacy-policy-url                     . "SDL_WINRT_PRIVACY_POLICY_URL")

    ;; Effective in SDL 2.0.4 and later
    (android-apk-expansion-main-file-version      . "SDL_ANDROID_APK_EXPANSION_MAIN_FILE_VERSION")
    (android-apk-expansion-patch-file-version     . "SDL_ANDROID_APK_EXPANSION_PATCH_FILE_VERSION")
    (android-separate-mouse-and-touch             . "SDL_ANDROID_SEPARATE_MOUSE_AND_TOUCH")
    (emscripten-keyboard-element                  . "SDL_EMSCRIPTEN_KEYBOARD_ELEMENT")
    (ime-internal-editing                         . "SDL_IME_INTERNAL_EDITING")
    (mac-background-app                           . "SDL_MAC_BACKGROUND_APP")
    (no-signal-handlers                           . "SDL_NO_SIGNAL_HANDLERS")
    (thread-stack-size                            . "SDL_THREAD_STACK_SIZE")
    (video-x11-net-wm-ping                        . "SDL_VIDEO_X11_NET_WM_PING")
    (windows-enable-messageloop                   . "SDL_WINDOWS_ENABLE_MESSAGELOOP")
    (windows-no-close-on-alt-f4                   . "SDL_WINDOWS_NO_CLOSE_ON_ALT_F4")
    (window-frame-usable-while-cursor-hidden      . "SDL_WINDOW_FRAME_USABLE_WHILE_CURSOR_HIDDEN")
    (xinput-use-old-joystick-mapping              . "SDL_XINPUT_USE_OLD_JOYSTICK_MAPPING")

    ;; Effective in SDL 2.0.5 and later
    (apple-tv-remote-allow-rotation               . "SDL_APPLE_TV_REMOTE_ALLOW_ROTATION")
    (bmp-save-legacy-format                       . "SDL_BMP_SAVE_LEGACY_FORMAT")
    (mouse-focus-clickthrough                     . "SDL_MOUSE_FOCUS_CLICKTHROUGH")
    (windows-disable-thread-naming                . "SDL_WINDOWS_DISABLE_THREAD_NAMING")

    ;; Effective in SDL 2.0.6 and later
    (audio-category                               . "SDL_AUDIO_CATEGORY")
    (audio-resampling-mode                        . "SDL_AUDIO_RESAMPLING_MODE")
    (mouse-normal-speed-scale                     . "SDL_MOUSE_NORMAL_SPEED_SCALE")
    (mouse-relative-speed-scale                   . "SDL_MOUSE_RELATIVE_SPEED_SCALE")
    (render-logical-size-mode                     . "SDL_RENDER_LOGICAL_SIZE_MODE")
    (touch-mouse-events                           . "SDL_TOUCH_MOUSE_EVENTS")
    (windows-intresource-icon                     . "SDL_WINDOWS_INTRESOURCE_ICON")
    (windows-intresource-icon-small               . "SDL_WINDOWS_INTRESOURCE_ICON_SMALL")

    ;; Effective in SDL 2.0.8 and later
    (ios-hide-home-indicator                      . "SDL_IOS_HIDE_HOME_INDICATOR")
    (return-key-hides-ime                         . "SDL_RETURN_KEY_HIDES_IME")
    (tv-remote-as-joystick                        . "SDL_TV_REMOTE_AS_JOYSTICK")
    (video-double-buffer                          . "SDL_VIDEO_DOUBLE_BUFFER")
    (video-x11-net-wm-bypass-compositor           . "SDL_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR")

    ;; Effective in SDL 2.0.9 and later
    (android-trap-back-button                     . "SDL_ANDROID_TRAP_BACK_BUTTON")
    (mouse-double-click-radius                    . "SDL_MOUSE_DOUBLE_CLICK_RADIUS")
    (mouse-double-click-time                      . "SDL_MOUSE_DOUBLE_CLICK_TIME")

    ;; Effective in SDL 2.0.10 and later
    (android-block-on-pause                       . "SDL_ANDROID_BLOCK_ON_PAUSE")
    (event-logging                                . "SDL_EVENT_LOGGING")
    (gamecontrollerconfig-file                    . "SDL_GAMECONTROLLERCONFIG_FILE")
    (mouse-touch-events                           . "SDL_MOUSE_TOUCH_EVENTS")
    (render-batching                              . "SDL_RENDER_BATCHING")

    ;; Effective in SDL 2.0.12 and later
    (display-usable-bounds                        . "SDL_DISPLAY_USABLE_BOUNDS")
    (gamecontroller-use-button-labels             . "SDL_GAMECONTROLLER_USE_BUTTON_LABELS")
    (gamecontrollertype                           . "SDL_GAMECONTROLLERTYPE")
    (joystick-hidapi-gamecube                     . "SDL_JOYSTICK_HIDAPI_GAMECUBE")
    (video-x11-force-egl                          . "SDL_VIDEO_X11_FORCE_EGL")
    (video-x11-window-visualid                    . "SDL_VIDEO_X11_WINDOW_VISUALID")

    ;; Effective in SDL 2.0.14 and later
    (android-block-on-pause-pauseaudio            . "SDL_ANDROID_BLOCK_ON_PAUSE_PAUSEAUDIO")
    (audio-device-app-name                        . "SDL_AUDIO_DEVICE_APP_NAME")
    (audio-device-stream-name                     . "SDL_AUDIO_DEVICE_STREAM_NAME")
    (emscripten-asyncify                          . "SDL_EMSCRIPTEN_ASYNCIFY")
    (joystick-hidapi-correlate-xinput             . "SDL_JOYSTICK_HIDAPI_CORRELATE_XINPUT")
    (joystick-hidapi-ps5                          . "SDL_JOYSTICK_HIDAPI_PS5")
    (joystick-rawinput                            . "SDL_JOYSTICK_RAWINPUT")
    (linux-joystick-deadzones                     . "SDL_LINUX_JOYSTICK_DEADZONES")
    (mouse-relative-scaling                       . "SDL_MOUSE_RELATIVE_SCALING")
    (preferred-locales                            . "SDL_PREFERRED_LOCALES")
    (thread-force-realtime-time-critical          . "SDL_THREAD_FORCE_REALTIME_TIME_CRITICAL")
    (thread-priority-policy                       . "SDL_THREAD_PRIORITY_POLICY")

    ;; Effective in SDL 2.0.16 and later
    (audio-device-stream-role                     . "SDL_AUDIO_DEVICE_STREAM_ROLE")
    (audio-include-monitors                       . "SDL_AUDIO_INCLUDE_MONITORS")
    ))
