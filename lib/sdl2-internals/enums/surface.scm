;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export symbol->blend-mode
        blend-mode->symbol)

(define-enum-mappings
  type: SDL_BlendMode
  symbol->value: symbol->blend-mode
  value->symbol: blend-mode->symbol

  ((required: libSDL-2.0.0+ "SDL_VERSION_ATLEAST(2,0,0)")
   (none   SDL_BLENDMODE_NONE)
   (blend  SDL_BLENDMODE_BLEND)
   (add    SDL_BLENDMODE_ADD)
   (mod    SDL_BLENDMODE_MOD))

  ((required: libSDL-2.0.12+ "SDL_VERSION_ATLEAST(2,0,12)")
   (mul    SDL_BLENDMODE_MUL)))


#+libSDL-2.0.6+
(begin
  (export symbol->blend-factor
          blend-factor->symbol

          symbol->blend-operation
          blend-operation->symbol)

  (define-enum-mappings
    type: SDL_BlendFactor
    symbol->value: symbol->blend-factor
    value->symbol: blend-factor->symbol

    ((zero                 SDL_BLENDFACTOR_ZERO)
     (one                  SDL_BLENDFACTOR_ONE)
     (src-color            SDL_BLENDFACTOR_SRC_COLOR)
     (one-minus-src-color  SDL_BLENDFACTOR_ONE_MINUS_SRC_COLOR)
     (src-alpha            SDL_BLENDFACTOR_SRC_ALPHA)
     (one-minus-src-alpha  SDL_BLENDFACTOR_ONE_MINUS_SRC_ALPHA)
     (dst-color            SDL_BLENDFACTOR_DST_COLOR)
     (one-minus-dst-color  SDL_BLENDFACTOR_ONE_MINUS_DST_COLOR)
     (dst-alpha            SDL_BLENDFACTOR_DST_ALPHA)
     (one-minus-dst-alpha  SDL_BLENDFACTOR_ONE_MINUS_DST_ALPHA)))

  (define-enum-mappings
    type: SDL_BlendOperation
    symbol->value: symbol->blend-operation
    value->symbol: blend-operation->symbol

    ((add           SDL_BLENDOPERATION_ADD)
     (subtract      SDL_BLENDOPERATION_SUBTRACT)
     (rev-subtract  SDL_BLENDOPERATION_REV_SUBTRACT)
     (minimum       SDL_BLENDOPERATION_MINIMUM)
     (maximum       SDL_BLENDOPERATION_MAXIMUM))))


#+libSDL-2.0.8+
(begin
  (export symbol->yuv-conversion-mode
          yuv-conversion-mode->symbol)

  (define-enum-mappings
    type: SDL_YUV_CONVERSION_MODE
    symbol->value: symbol->yuv-conversion-mode
    value->symbol: yuv-conversion-mode->symbol

    ((jpeg       SDL_YUV_CONVERSION_JPEG)
     (bt601      SDL_YUV_CONVERSION_BT601)
     (bt709      SDL_YUV_CONVERSION_BT709)
     (automatic  SDL_YUV_CONVERSION_AUTOMATIC))))
