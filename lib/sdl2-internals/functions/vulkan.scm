;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–20121  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


#+libSDL-2.0.6+
(begin
  (export SDL_Vulkan_LoadLibrary
          SDL_Vulkan_UnloadLibrary
          SDL_Vulkan_GetVkGetInstanceProcAddr
          SDL_Vulkan_GetInstanceExtensions
          SDL_Vulkan_GetInstanceExtensions__Names
          SDL_Vulkan_GetDrawableSize
          SDL_Vulkan_CreateSurface)


  (define-function-binding SDL_Vulkan_LoadLibrary
    return: (Sint32 zero-on-success)
    args: (((const c-string) path)))

  (define-function-binding SDL_Vulkan_UnloadLibrary)

  (define-function-binding SDL_Vulkan_GetVkGetInstanceProcAddr
    return: (c-pointer void))

  (define-function-binding SDL_Vulkan_GetInstanceExtensions
    return: (bool success)
    args: ((SDL_Window* window)
           (Uint32* pCount_out)
           ((const (c-pointer c-string)) pNames_out)))

  (define-function-binding* SDL_Vulkan_GetInstanceExtensions__Names
    return: (c-string-list names)
    args: ((SDL_Window* window)
           (Uint8* success_out))
    body: "unsigned int pCount = 0;
           SDL_bool success;

           /* Fill pCount */
           success = SDL_Vulkan_GetInstanceExtensions(window, &pCount, NULL);

           if (SDL_FALSE == success) {
             *success_out = 0;
             C_return(NULL);
           }

           /* Alloc 1 extra so the list is null-terminated, which allows
            * CHICKEN to detect the end of the string list. */
           const char** pNames = SDL_malloc((1 + pCount) * sizeof(char*));

           /* Fill pNames */
           success = SDL_Vulkan_GetInstanceExtensions(window, &pCount, pNames);

           if (SDL_FALSE == success) {
             *success_out = 0;
             C_return(NULL);
           }

           *success_out = 1;
           C_return(pNames);")

  (define-function-binding SDL_Vulkan_GetDrawableSize
    args: ((SDL_Window* window)
           (Sint32* w-out)
           (Sint32* h-out)))

  (define-function-binding* SDL_Vulkan_CreateSurface
    return: (bool success)
    args: ((SDL_Window* window)
           (VkInstance* instance)
           (VkSurfaceKHR* surface))
    body: "C_return(SDL_Vulkan_CreateSurface(window, *instance, surface));")

  )
