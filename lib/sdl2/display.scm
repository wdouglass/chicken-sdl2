;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export video-init!
        video-quit!
        get-num-video-drivers
        get-video-driver
        get-current-video-driver

        get-num-video-displays
        get-display-name
        get-display-dpi ;; SDL 2.0.4+

        get-display-bounds
        get-display-usable-bounds

        screen-saver-enabled?
        screen-saver-enabled-set!

        get-num-display-modes
        get-display-mode
        get-current-display-mode
        get-closest-display-mode
        get-desktop-display-mode

        get-display-orientation ;; SDL 2.0.9+
        get-display-orientation-raw ;; SDL 2.0.9+
        )


(: video-init!
   (#!optional (or string false) -> void))
(define (video-init! #!optional driver-name)
  (try-call (SDL_VideoInit driver-name))
  (void))

(: video-quit!
   (-> void))
(define (video-quit!)
  (SDL_VideoQuit))

(: get-num-video-drivers
   (--> fixnum))
(define (get-num-video-drivers)
  (try-call (SDL_GetNumVideoDrivers)))

(: get-video-driver
   (fixnum --> string))
(define (get-video-driver index)
  (SDL_GetVideoDriver index))

(: get-current-video-driver
   (--> (or string false)))
(define (get-current-video-driver)
  (SDL_GetCurrentVideoDriver))


(: get-num-video-displays
   (--> fixnum))
(define (get-num-video-displays)
  (try-call (SDL_GetNumVideoDisplays)))

(: get-display-name
   (fixnum --> string))
(define (get-display-name display-index)
  (try-call (SDL_GetDisplayName display-index)
            fail?: not))

#+libSDL-2.0.4+
(: get-display-dpi
   (fixnum --> float float float))
(define-versioned (get-display-dpi display-index)
    libSDL-2.0.4+
  (with-temp-mem ((ddpi-out (%allocate-float))
                  (hdpi-out (%allocate-float))
                  (vdpi-out (%allocate-float)))
    (try-call (SDL_GetDisplayDPI
               display-index ddpi-out hdpi-out vdpi-out)
              on-fail: (begin (free ddpi-out)
                              (free hdpi-out)
                              (free vdpi-out)))
    (values (pointer-f32-ref ddpi-out)
            (pointer-f32-ref hdpi-out)
            (pointer-f32-ref vdpi-out))))


(: get-display-bounds
   (integer #!optional sdl2:rect* -> sdl2:rect))
(define (get-display-bounds display #!optional (rect-out (make-rect)))
  (try-call (SDL_GetDisplayBounds display rect-out))
  rect-out)

#+libSDL-2.0.5+
(: get-display-usable-bounds
   (integer #!optional sdl2:rect* -> sdl2:rect))
(define-versioned (get-display-usable-bounds
                   display #!optional (rect-out (make-rect)))
    libSDL-2.0.5+
  (try-call (SDL_GetDisplayUsableBounds display rect-out))
  rect-out)


(: screen-saver-enabled?
   (-> boolean))
(define (screen-saver-enabled?)
  (SDL_IsScreenSaverEnabled))

(: screen-saver-enabled-set!
   (boolean -> void))
(define (screen-saver-enabled-set! enabled?)
  (case enabled?
    ((#t) (SDL_EnableScreenSaver))
    ((#f) (SDL_DisableScreenSaver))
    (else (error 'screen-saver-enabled-set!
                 "not a boolean" enabled?))))

(set! (setter screen-saver-enabled?)
      screen-saver-enabled-set!)


(: get-num-display-modes
   (fixnum --> fixnum))
(define (get-num-display-modes display-index)
  (try-call (SDL_GetNumDisplayModes display-index)))

(: get-display-mode
   (fixnum fixnum --> sdl2:display-mode))
(define (get-display-mode display-index mode-index)
  (let ((mode-out (alloc-display-mode)))
    (try-call (SDL_GetDisplayMode display-index mode-index mode-out)
              on-fail: (free-display-mode! mode-out))
    mode-out))

(: get-current-display-mode
   (fixnum --> sdl2:display-mode))
(define (get-current-display-mode display-index)
  (let ((mode-out (alloc-display-mode)))
    (try-call (SDL_GetCurrentDisplayMode display-index mode-out)
              on-fail: (free-display-mode! mode-out))
    mode-out))

(: get-closest-display-mode
   (fixnum sdl2:display-mode* --> sdl2:display-mode))
(define (get-closest-display-mode display-index target-mode)
  (let ((mode-out (alloc-display-mode)))
    (try-call (SDL_GetClosestDisplayMode
               display-index target-mode mode-out)
              fail?: (%struct-fail display-mode?))
    mode-out))

(: get-desktop-display-mode
   (fixnum --> sdl2:display-mode))
(define (get-desktop-display-mode display-index)
  (let ((mode-out (alloc-display-mode)))
    (try-call (SDL_GetDesktopDisplayMode display-index mode-out)
              on-fail: (free-display-mode! mode-out))
    mode-out))


#+libSDL-2.0.9+
(: get-display-orientation
   (integer --> symbol))
(define-versioned (get-display-orientation display-index)
    libSDL-2.0.9+
  (display-orientation->symbol
   (get-display-orientation-raw display-index)))

#+libSDL-2.0.9+
(: get-display-orientation-raw
   (integer --> integer))
(define-versioned (get-display-orientation-raw display-index)
    libSDL-2.0.9+
  (SDL_GetDisplayOrientation display-index))
