;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export game-controller-open!
        game-controller-close!
        game-controller-from-instance-id
        game-controller-update!

        game-controller-event-state?
        game-controller-event-state-set!

        game-controller-attached?
        game-controller-get-joystick
        is-game-controller?

        game-controller-mapping
        game-controller-mapping-for-device-index
        game-controller-mapping-for-guid

        game-controller-add-mapping!
        game-controller-add-mappings-from-file!
        game-controller-add-mappings-from-rw!

        game-controller-name
        game-controller-name-for-index

        game-controller-get-axis
        game-controller-get-axis-from-string
        game-controller-get-axis-from-string-raw
        game-controller-get-string-for-axis

        game-controller-get-button
        game-controller-get-button-from-string
        game-controller-get-button-from-string-raw
        game-controller-get-string-for-button

        game-controller-get-bind-for-axis
        game-controller-get-bind-for-button

        game-controller-get-vendor
        game-controller-get-product
        game-controller-get-product-version
        game-controller-get-player-index

        game-controller-num-mappings
        game-controller-mapping-for-index

        game-controller-rumble!)


(: game-controller-open!
   (fixnum -> sdl2:game-controller))
(define (game-controller-open! index)
  (try-call (SDL_GameControllerOpen index)
            fail?: (%struct-fail game-controller?)))

(: game-controller-close!
   (sdl2:game-controller -> void))
(define (game-controller-close! controller)
  (SDL_GameControllerClose controller))

#+libSDL-2.0.4+
(: game-controller-from-instance-id
   (integer -> sdl2:game-controller*))
(define-versioned (game-controller-from-instance-id joy-id)
    libSDL-2.0.4+
  (try-call (SDL_GameControllerFromInstanceID joy-id)
            fail?: (%struct-fail game-controller?)))


(: game-controller-update!
   (-> void))
(define (game-controller-update!)
  (SDL_GameControllerUpdate))


(: game-controller-event-state?
   (--> boolean))
(define (game-controller-event-state?)
  (= (SDL_GameControllerEventState SDL_QUERY)
     SDL_ENABLE))

(: game-controller-event-state-set!
   (boolean -> boolean))
(define (game-controller-event-state-set! state)
  (let ((state-int
         (case state
           ((#t) SDL_ENABLE)
           ((#f) SDL_IGNORE)
           (else (error 'game-controller-event-state-set!
                        "invalid state" state)))))
    (= (try-call (SDL_GameControllerEventState state-int))
       SDL_ENABLE)))

(set! (setter game-controller-event-state?)
      game-controller-event-state-set!)


(: game-controller-attached?
   (sdl2:game-controller* --> boolean))
(define (game-controller-attached? controller)
  (SDL_GameControllerGetAttached controller))

(: game-controller-get-joystick
   (sdl2:game-controller* --> sdl2:joystick))
(define (game-controller-get-joystick controller)
  (SDL_GameControllerGetJoystick controller))

(: is-game-controller?
   (fixnum --> boolean))
(define (is-game-controller? index)
  (SDL_IsGameController index))


(: game-controller-mapping
   (sdl2:game-controller* --> (or string false)))
(define (game-controller-mapping controller)
  (SDL_GameControllerMapping controller))

#+libSDL-2.0.9+
(: game-controller-mapping-for-device-index
   (fixnum --> (or string false)))
(define-versioned (game-controller-mapping-for-device-index index)
    libSDL-2.0.9+
  (SDL_GameControllerMappingForDeviceIndex index))

(: game-controller-mapping-for-guid
   (sdl2:joystick-guid --> (or string false)))
(define (game-controller-mapping-for-guid guid)
  (SDL_GameControllerMappingForGUID guid))


(: game-controller-add-mapping!
   (string -> fixnum))
(define (game-controller-add-mapping! mapping)
  (try-call (SDL_GameControllerAddMapping mapping)))

#+libSDL-2.0.2+
(: game-controller-add-mappings-from-file!
   (string -> number))
(define-versioned (game-controller-add-mappings-from-file! filename)
    libSDL-2.0.2+
  (try-call (SDL_GameControllerAddMappingsFromFile filename)))

#+libSDL-2.0.2+
(: game-controller-add-mappings-from-rw!
   (sdl2:rwops boolean -> number))
(define-versioned (game-controller-add-mappings-from-rw! rwops close?)
    libSDL-2.0.2+
  (try-call (SDL_GameControllerAddMappingsFromRW rwops close?)))


(: game-controller-name
   (sdl2:game-controller* --> string))
(define (game-controller-name controller)
  (try-call (SDL_GameControllerName controller)
            fail?: (lambda (s) (= 0 (string-length s)))))

(: game-controller-name-for-index
   (fixnum --> string))
(define (game-controller-name-for-index device-index)
  (try-call (SDL_GameControllerNameForIndex device-index)
            fail?: (lambda (s) (= 0 (string-length s)))))


(: game-controller-get-axis
   (sdl2:game-controller* enum --> integer))
(define (game-controller-get-axis controller axis)
  (let ((axis-int
         (if (integer? axis)
             axis
             (symbol->game-controller-axis axis))))
    (SDL_GameControllerGetAxis controller axis-int)))

(: game-controller-get-axis-from-string
   (string --> symbol))
(define (game-controller-get-axis-from-string str)
  (game-controller-axis->symbol
   (game-controller-get-axis-from-string-raw str)))

(: game-controller-get-axis-from-string-raw
   (string --> integer))
(define (game-controller-get-axis-from-string-raw str)
  (SDL_GameControllerGetAxisFromString str))

(: game-controller-get-string-for-axis
   (enum --> string))
(define (game-controller-get-string-for-axis axis)
  (let ((axis-int
         (if (integer? axis)
             axis
             (symbol->game-controller-axis axis))))
    (SDL_GameControllerGetStringForAxis axis-int)))


(: game-controller-get-button
   (sdl2:game-controller* enum --> boolean))
(define (game-controller-get-button controller button)
  (let ((button-int
         (if (integer? button)
             button
             (symbol->game-controller-button button))))
    (SDL_GameControllerGetButton controller button-int)))

(: game-controller-get-button-from-string
   (string --> symbol))
(define (game-controller-get-button-from-string str)
  (game-controller-button->symbol
   (game-controller-get-button-from-string-raw str)))

(: game-controller-get-button-from-string-raw
   (string --> integer))
(define (game-controller-get-button-from-string-raw str)
  (SDL_GameControllerGetButtonFromString str))

(: game-controller-get-string-for-button
   (enum --> string))
(define (game-controller-get-string-for-button button)
  (let ((button-int
         (if (integer? button)
             button
             (symbol->game-controller-button button))))
    (SDL_GameControllerGetStringForButton button-int)))


(: game-controller-get-bind-for-axis
   (sdl2:game-controller* enum --> sdl2:game-controller-button-bind))
(define (game-controller-get-bind-for-axis controller axis)
  (let ((axis-int
         (if (integer? axis)
             axis
             (symbol->game-controller-axis axis))))
    (SDL_GameControllerGetBindForAxis controller axis-int)))

(: game-controller-get-bind-for-button
   (sdl2:game-controller* enum --> sdl2:game-controller-button-bind))
(define (game-controller-get-bind-for-button controller button)
  (let ((button-int
         (if (integer? button)
             button
             (symbol->game-controller-button button))))
    (SDL_GameControllerGetBindForButton controller button-int)))


#+libSDL-2.0.6+
(: game-controller-get-vendor
   (sdl2:game-controller --> integer))
(define-versioned (game-controller-get-vendor controller)
    libSDL-2.0.6+
  (SDL_GameControllerGetVendor controller))

#+libSDL-2.0.6+
(: game-controller-get-product
   (sdl2:game-controller --> integer))
(define-versioned (game-controller-get-product controller)
    libSDL-2.0.6+
  (SDL_GameControllerGetProduct controller))

#+libSDL-2.0.6+
(: game-controller-get-product-version
   (sdl2:game-controller --> integer))
(define-versioned (game-controller-get-product-version controller)
    libSDL-2.0.6+
  (SDL_GameControllerGetProductVersion controller))

#+libSDL-2.0.9+
(: game-controller-get-player-index
   (sdl2:game-controller* --> integer))
(define-versioned (game-controller-get-player-index controller)
    libSDL-2.0.9+
  (SDL_GameControllerGetPlayerIndex controller))


#+libSDL-2.0.6+
(: game-controller-num-mappings
   (--> integer))
(define-versioned (game-controller-num-mappings)
    libSDL-2.0.6+
  (SDL_GameControllerNumMappings))

#+libSDL-2.0.6+
(: game-controller-mapping-for-index
   (integer --> (or string false)))
(define-versioned (game-controller-mapping-for-index index)
    libSDL-2.0.6+
  (SDL_GameControllerMappingForIndex index))


#+libSDL-2.0.9+
(: game-controller-rumble!
   (sdl2:game-controller* integer integer integer -> boolean))
(define-versioned (game-controller-rumble!
                   controller low-freq hi-freq duration-ms)
    libSDL-2.0.9+
  (= 0 (SDL_GameControllerRumble
        controller low-freq hi-freq duration-ms)))
