;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export init!
        init-subsystem!
        quit!
        quit-subsystem!
        was-init
        set-main-ready!

        clear-error!
        get-error
        set-error!

        get-platform
        get-base-path ;; SDL 2.0.1+
        get-pref-path ;; SDL 2.0.1+
        is-tablet? ;; SDL 2.0.9+
        get-power-info

        show-simple-message-box

        has-clipboard-text?
        get-clipboard-text
        set-clipboard-text!

        open-url)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION AND SHUTDOWN

(: init!
   (#!optional enum-list -> void))
(define (init! #!optional (flags '(everything)))
  (try-call (SDL_Init (pack-init-flags flags)))
  (void))

(: init-subsystem!
   (enum-list -> void))
(define (init-subsystem! flags)
  (try-call (SDL_InitSubSystem (pack-init-flags flags)))
  (void))

(: quit!
   (-> void))
(define (quit!)
  (SDL_Quit))

(: quit-subsystem!
   (enum-list -> void))
(define (quit-subsystem! flags)
  (SDL_QuitSubSystem (pack-init-flags flags)))

(: was-init
   (#!optional enum-list -> (list-of symbol)))
(define (was-init #!optional (flags '(everything)))
  (unpack-init-flags
   (SDL_WasInit (pack-init-flags flags))
   #t))

(: set-main-ready!
   (-> void))
(define (set-main-ready!)
  (SDL_SetMainReady))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; ERROR HANDLING

(: clear-error!
   (-> void))
(define (clear-error!)
  (SDL_ClearError))

(: get-error
   (-> string))
(define (get-error)
  (SDL_GetError))

(: set-error!
   (string -> void))
(define (set-error! message)
  (SDL_SetError message))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PLATFORM / HARDWARE

(: get-platform
   (-> string))
(define (get-platform)
  (SDL_GetPlatform))


#+libSDL-2.0.1+
(: get-base-path
   (-> string))
(define-versioned (get-base-path)
    libSDL-2.0.1+
  (SDL_GetBasePath))

#+libSDL-2.0.1+
(: get-pref-path
   (string string -> string))
(define-versioned (get-pref-path org app)
    libSDL-2.0.1+
  (SDL_GetPrefPath org app))


#+libSDL-2.0.9+
(: is-tablet?
   (--> boolean))
(define-versioned (is-tablet?)
    libSDL-2.0.9+
  (SDL_IsTablet))


(: get-power-info
   (-> symbol integer fixnum))
(define (get-power-info)
  (with-temp-mem ((secs-out (%allocate-Sint32))
                  (pct-out (%allocate-Sint32)))
    (let ((state (SDL_GetPowerInfo secs-out pct-out)))
      (values (power-state->symbol state)
              (pointer-s32-ref secs-out)
              (pointer-s32-ref pct-out)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MESSAGE BOX

(: show-simple-message-box
   (enum string string #!optional (or sdl2:window* false) -> void))
(define (show-simple-message-box flag title message #!optional window)
  (try-call (SDL_ShowSimpleMessageBox
             (if (integer? flag)
                 flag
                 (symbol->message-box-flag flag))
             title message
             (or window )))
  (void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CLIP BOARD

(: has-clipboard-text?
   (-> boolean))
(define (has-clipboard-text?)
  (SDL_HasClipboardText))

(: get-clipboard-text
   (-> string))
(define (get-clipboard-text)
  (try-call (SDL_GetClipboardText)
            fail?: not))

(: set-clipboard-text!
   (string -> void))
(define (set-clipboard-text! text)
  (try-call (SDL_SetClipboardText text))
  (void))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MISC

#+libSDL-2.0.14+
(: open-url
   (string -> void))
(define-versioned (open-url url)
    libSDL-2.0.14+
  (try-call (SDL_OpenURL url))
  (void))
