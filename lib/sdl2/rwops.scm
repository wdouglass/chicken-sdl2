;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export rw-from-file
        rw-from-const-mem
        rw-from-mem

        rw-from-blob
        rw-from-string

        rw-close!)


(: rw-from-file
   (string string -> sdl2:rwops))
(define (rw-from-file path mode)
  (try-call (SDL_RWFromFile path mode)
            fail?: (%struct-fail rwops?)))

(: rw-from-const-mem
   ((or pointer locative) fixnum -> sdl2:rwops))
(define (rw-from-const-mem pointer size)
  (try-call (SDL_RWFromConstMem pointer size)
            fail?: (%struct-fail rwops?)))

(: rw-from-mem
   ((or pointer locative) fixnum -> sdl2:rwops))
(define (rw-from-mem pointer size)
  (try-call (SDL_RWFromMem pointer size)
            fail?: (%struct-fail rwops?)))


(: rw-from-blob
   (blob -> sdl2:rwops))
(define (rw-from-blob blob)
  (try-call (SDL_RWFromMem (make-locative blob) (blob-size blob))
            fail?: (%struct-fail rwops?)))

(: rw-from-string
   (string -> sdl2:rwops))
(define (rw-from-string str)
  (try-call (SDL_RWFromMem (make-locative str) (string-length str))
            fail?: (%struct-fail rwops?)))


(: rw-close!
   (sdl2:rwops* -> void))
(define (rw-close! rwops)
  (try-call (SDL_RWclose rwops))
  (%nullify-struct! rwops))
