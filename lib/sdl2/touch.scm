;;
;; chicken-sdl2: CHICKEN Scheme bindings to Simple DirectMedia Layer 2
;;
;; Copyright © 2013–2021  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export get-num-touch-devices
        get-touch-device

        get-touch-device-type
        get-touch-device-type-raw

        get-num-touch-fingers
        get-touch-finger

        ;; TODO: record-gesture!
        ;; TODO: save-dollar-template!
        ;; TODO: save-all-dollar-templates!
        ;; TODO: load-dollar-templates
        )


(: get-num-touch-devices
   (-> fixnum))
(define (get-num-touch-devices)
  (SDL_GetNumTouchDevices))

(: get-touch-device
   (fixnum -> integer))
(define (get-touch-device device-id)
  (try-call (SDL_GetTouchDevice device-id)
            fail?: zero?))


#+libSDL-2.0.10+
(: get-touch-device-type
   (integer -> symbol))
(define-versioned (get-touch-device-type touch-id)
    libSDL-2.0.10+
  (touch-device-type->symbol
   (get-touch-device-type-raw touch-id)))

#+libSDL-2.0.10+
(: get-touch-device-type-raw
   (integer -> fixnum))
(define-versioned (get-touch-device-type-raw touch-id)
    libSDL-2.0.10+
  (try-call (SDL_GetTouchDeviceType touch-id)
            fail?: zero?))


(: get-num-touch-fingers
   (integer -> fixnum))
(define (get-num-touch-fingers touch-id)
  (SDL_GetNumTouchFingers touch-id))

(: get-touch-finger
   (fixnum fixnum -> sdl2:finger))
(define (get-touch-finger touch-id index)
  (try-call (SDL_GetTouchFinger touch-id index)
            fail?: (%struct-fail finger?)))


;; TODO: record-gesture!
;; TODO: save-dollar-template!
;; TODO: save-all-dollar-templates!
;; TODO: load-dollar-templates
