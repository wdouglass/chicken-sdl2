
(define (colors-list ncolors)
  (map (lambda (i) (sdl2:make-color i (- 255 i) (random-int 255)))
       (iota ncolors)))

(define (assert-palette-entry pal i expected)
  (let ((actual (sdl2:palette-ref pal i)))
    (assert (sdl2:color=? expected actual)
            (sprintf "Expected palette index ~A to be ~A but got ~A"
                     i expected actual))))


(test-group "sdl2:make-palette"
  (test-assert "Creates a palette"
    (sdl2:palette? (sdl2:make-palette)))
  (test "Has 256 colors by default"
        256
        (sdl2:palette-ncolors (sdl2:make-palette)))
  (test "Allows caller to specify number of colors"
        42
        (sdl2:palette-ncolors (sdl2:make-palette 42)))
  (test "Allows as few as 1 color"
        1
        (sdl2:palette-ncolors (sdl2:make-palette 1)))
  (test "Allows more than 256 colors"
        1024
        (sdl2:palette-ncolors (sdl2:make-palette 1024)))
  (test-error "Raises error if number of colors is zero"
    (sdl2:make-palette 0))
  (test-error "Raises error if number of colors is negative"
    (sdl2:make-palette -1)))


(test-group "sdl2:palette-ref / sdl2:palette-set!"
  (define (setup #!optional (ncolors 256))
    (let* ((colors (colors-list ncolors))
           (src (sdl2:make-surface ncolors 1 8))
           (pal (sdl2:surface-palette src))
           (dst (sdl2:make-surface ncolors 1 32)))
      (for-each (lambda (i)
                  (sdl2:palette-set! pal i (list-ref colors i))
                  (sdl2:surface-set! src i 0 i))
                (iota ncolors))
      (sdl2:blit-surface! src #f dst #f)
      (values colors src pal dst)))

  (test-assert "Setting palette affects the surface colors"
    (let-values (((colors src pal dst) (setup 256)))
      (every (lambda (i expected)
               (let ((actual (sdl2:surface-ref dst i 0)))
                 (assert (sdl2:color=? expected actual)
                         (sprintf "Expected surface pixel (~A,0) to be ~A but got ~A"
                                  i expected actual))))
             (iota 256)
             colors)))

  (test-group "sdl2:palette-ref"
    (test-assert "Returns the color at the given index"
      (let-values (((colors src pal dst) (setup 256)))
        (every (lambda (i expected)
                 (assert-palette-entry pal i expected))
               (iota 256)
               colors)))

    (test-error "Raises error if index is too small"
      (sdl2:palette-ref (sdl2:make-palette 128) -1))

    (test-error "Raises error if index is too big"
      (sdl2:palette-ref (sdl2:make-palette 128) 128)))

  (test-group "sdl2:palette-set!"
    (let ((pal (sdl2:make-palette)))
      (sdl2:palette-set! pal 42 (sdl2:make-color 1 7 3 2))
      (test "Overwrites the color at the given index"
            '(1 7 3 2)
            (sdl2:color->list (sdl2:palette-ref pal 42))))

    (test-error "Raises error if index is too small"
      (sdl2:palette-set! (sdl2:make-palette 128) -1 (sdl2:make-color)))

    (test-error "Raises error if index is too big"
      (sdl2:palette-set! (sdl2:make-palette 128) 128 (sdl2:make-color))))

  (test-group "(set! (sdl2:palette-ref))"
    (let ((pal (sdl2:make-palette)))
      (set! (sdl2:palette-ref pal 42) (sdl2:make-color 1 7 3 2))
      (test "Overwrites the color at the given index"
            '(1 7 3 2)
            (sdl2:color->list (sdl2:palette-ref pal 42))))

    (test-error "Raises error if index is too small"
      (set! (sdl2:palette-ref (sdl2:make-palette 128) -1) (sdl2:make-color)))

    (test-error "Raises error if index is too big"
      (set! (sdl2:palette-ref (sdl2:make-palette 128) 128) (sdl2:make-color)))))


(test-group "sdl2:palette-colors"
  (define (setup #!optional (ncolors 256))
    (let* ((colors (colors-list ncolors))
           (pal (sdl2:make-palette ncolors)))
      (for-each (lambda (i)
                  (sdl2:palette-set! pal i (list-ref colors i)))
                (iota ncolors))
      (values colors pal)))

  (define (do-test n)
    (let-values (((colors pal) (setup n)))
      (let ((vcolors (sdl2:palette-colors pal)))
        (and (assert (= n (vector-length vcolors))
                     (sprintf "Expected palette to have ~A colors but got ~A"
                              n (vector-length vcolors)))
             (every (lambda (i expected)
                      (let ((actual (vector-ref vcolors i)))
                        (assert (sdl2:color=? expected actual)
                                (sprintf "Expected palette index ~A to be ~A but got ~A"
                                         i expected actual))))
                    (iota n)
                    colors)))))

  (test-assert "Returns a vector of all the palette colors"
    (do-test 256))

  (test-assert "Returns a smaller vector if there are fewer colors"
    (do-test 42)))


(test-group "sdl2:palette-colors-set!"
  (define (setup #!optional (ncolors 256))
    (let* ((colors (colors-list ncolors))
           (pal (sdl2:make-palette ncolors)))
      (for-each (lambda (i)
                  (sdl2:palette-set! pal i (sdl2:make-color i 0 0)))
                (iota ncolors))
      (values colors pal)))

  (test-assert "Overwrites multiple palette colors"
    (let-values (((colors pal) (setup 256)))
      (sdl2:palette-colors-set! pal (list->vector colors))
      (every (lambda (i expected)
               (assert-palette-entry pal i expected))
             (iota 256)
             colors)))

  (test-assert "Overwrites only as many colors as given"
    (let-values (((colors pal) (setup 256)))
      (sdl2:palette-colors-set! pal (list->vector (take colors 42)))
      (and
       ;; Entries 0-41 have been overwritten by the palette.
       (every (lambda (i expected)
                (assert-palette-entry pal i expected))
              (iota 42 0)
              colors)
       ;; Entries 42-255 have remained the original colors
       (every (lambda (i)
                (assert-palette-entry pal i (sdl2:make-color i 0 0)))
              (iota (- 256 42) 42)))))

  (test-assert "Can start from a given offset"
    (let-values (((colors pal) (setup 256)))
      (sdl2:palette-colors-set! pal (list->vector (take colors 42)) 100)
      (and
       ;; Entries 0-99 have remained the original colors
       (every (lambda (i)
                (assert-palette-entry pal i (sdl2:make-color i 0 0)))
              (iota 100 0))
       ;; Entries 101-142 have been overwritten by the palette.
       (every (lambda (i expected)
                (assert-palette-entry pal i expected))
              (iota 42 100)
              (take colors 42))
       ;; Entries 143-255 have remained the original colors
       (every (lambda (i)
                (assert-palette-entry pal i (sdl2:make-color i 0 0)))
              (iota (- 256 143) 143)))))

  (test-assert "Returns #t if all given colors were used"
    (let-values (((colors pal) (setup 256)))
      (eq? #t (sdl2:palette-colors-set! pal (list->vector colors)))))

  (test-assert "Returns #t if all colors from a small vec were used"
    (let-values (((colors pal) (setup 256)))
      (eq? #t (sdl2:palette-colors-set! pal (list->vector (take colors 42))))))

  (test-assert "Returns #f if any given colors were NOT used"
    (let-values (((colors pal) (setup 256)))
      (eq? #f (sdl2:palette-colors-set! pal (list->vector (take colors 2)) 255)))))


(test-group "(set! (sdl2:palette-colors))"
  (define (setup #!optional (ncolors 256))
    (let* ((colors (colors-list ncolors))
           (pal (sdl2:make-palette ncolors)))
      (for-each (lambda (i)
                  (sdl2:palette-set! pal i (sdl2:make-color i 0 0)))
                (iota ncolors))
      (values colors pal)))

  (test-assert "Overwrites multiple palette colors"
    (let-values (((colors pal) (setup 256)))
      (set! (sdl2:palette-colors pal) (list->vector colors))
      (every (lambda (i expected)
               (assert-palette-entry pal i expected))
             (iota 256)
             colors)))

  (test-assert "Overwrites only as many colors as given"
    (let-values (((colors pal) (setup 256)))
      (set! (sdl2:palette-colors pal) (list->vector (take colors 42)))
      (and
       ;; Entries 0-41 have been overwritten by the palette.
       (every (lambda (i expected)
                (assert-palette-entry pal i expected))
              (iota 42 0)
              colors)
       ;; Entries 42-255 have remained the original colors
       (every (lambda (i)
                (assert-palette-entry pal i (sdl2:make-color i 0 0)))
              (iota (- 256 42) 42)))))

  (test-assert "Returns #t if all given colors were used"
    (let-values (((colors pal) (setup 256)))
      (eq? #t (set! (sdl2:palette-colors pal) (list->vector colors)))))

  (test-assert "Returns #t if all colors from a small vec were used"
    (let-values (((colors pal) (setup 256)))
      (eq? #t (set! (sdl2:palette-colors pal) (list->vector (take colors 42))))))

  (test-assert "Returns #f if any given colors were NOT used"
    (let-values (((colors pal) (setup 256)))
      (eq? #f (set! (sdl2:palette-colors pal)
                (list->vector (cons (sdl2:make-color) colors)))))))


(test-group "aliases"
  (test-assert "sdl2:palette-ncolours is an alias"
    (eq? sdl2:palette-ncolours sdl2:palette-ncolors))
  (test-assert "sdl2:palette-colours is an alias"
    (eq? sdl2:palette-colours sdl2:palette-colors))
  (test-assert "sdl2:palette-colours-set! is an alias"
    (eq? sdl2:palette-colours-set! sdl2:palette-colors-set!)))
