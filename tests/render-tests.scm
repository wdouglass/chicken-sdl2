
(versioned-test-group "sdl2:render-clip-enabled?"
    libSDL-2.0.4+
  (sdl2:quit!)
  (sdl2:init! '(video))

  (receive (window renderer)
      (sdl2:create-window-and-renderer! 100 100 '(hidden))
    (test "Is #f by default"
          #f
          (sdl2:render-clip-enabled? renderer))

    (test "Is #t after setting clip rect to a non-empty rect"
          #t
          (begin
            (set! (sdl2:render-clip-rect renderer)
              (sdl2:make-rect 1 2 3 4))
            (sdl2:render-clip-enabled? renderer)))

    (test "Is #f after setting clip rect to #f"
          #f
          (begin
            (set! (sdl2:render-clip-rect renderer) #f)
            (sdl2:render-clip-enabled? renderer)))

    (test "Is #t after setting clip rect to an empty rect"
          #t
          (begin
            (set! (sdl2:render-clip-rect renderer)
              (sdl2:make-rect 0 0 0 0))
            (sdl2:render-clip-enabled? renderer)))

    (sdl2:destroy-renderer! renderer)
    (sdl2:destroy-window! window)))


(versioned-test-group "sdl2:render-integer-scale?"
    libSDL-2.0.5+
  (receive (window renderer)
      (sdl2:create-window-and-renderer! 10 10 '(hidden))
    (test "Returns whether renderer forces integer scales"
          #f
          (sdl2:render-integer-scale? renderer))

    (set! (sdl2:render-integer-scale? renderer) #t)
    (test "Can be set!"
          #t
          (sdl2:render-integer-scale? renderer))))


(versioned-test-group "sdl2:render-integer-scale-set!"
    libSDL-2.0.5+
  (receive (window renderer)
      (sdl2:create-window-and-renderer! 10 10 '(hidden))
    (sdl2:render-integer-scale-set! renderer #t)
    (test "Sets whether renderer forces integer scales"
          #t
          (sdl2:render-integer-scale? renderer))))
